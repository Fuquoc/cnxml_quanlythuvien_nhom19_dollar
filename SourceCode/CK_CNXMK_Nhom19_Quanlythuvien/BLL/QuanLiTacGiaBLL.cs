﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;

namespace CK_CNXMK_Nhom19_Quanlythuvien.BLL
{
    class QuanLiTacGiaBLL
    {
        QuanLiTacGiaDAL qldg_dal;
        public QuanLiTacGiaBLL()
        {
            qldg_dal = new QuanLiTacGiaDAL();
        }
        public DataTable getTacGia()
        {
            return qldg_dal.getTacGia();
        }
        public bool InsertTacGia(TacGia tg)
        {
            return qldg_dal.InsertTacGia(tg);
        }
        public bool UpdateTacGIa(TacGia tg)
        {
            return qldg_dal.UpdateTacGIa(tg);
        }
        public bool DeleteTacGia(TacGia tg)
        {
            return qldg_dal.DeleteTacGia(tg);
        }



        public DataTable FindTacGia(string dg)
        {
            return qldg_dal.FindTacGia(dg);
        }

    }
}
