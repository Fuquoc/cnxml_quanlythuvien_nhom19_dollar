﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;

namespace CK_CNXMK_Nhom19_Quanlythuvien.BLL
{
    class CapNhatTraSachBLL
    {
        CapNhatTraSachDAL cnms_dal;

        public CapNhatTraSachBLL()
        {
            cnms_dal = new CapNhatTraSachDAL();
        }

        public DataTable getTraSach()
        {
            return cnms_dal.getTraSach();
        }

        public DataTable XuatMaSach()
        {
            return cnms_dal.XuatMaSach();
        }
       


        public DataTable XuatDG()
        {
            return cnms_dal.XuatDG();
        }

        public bool InsertTraSach(MuonSach sach)
        {
            return cnms_dal.InsertTraSach(sach);
        }
        public bool UpdateTraSach(MuonSach sach)
        {
            return cnms_dal.UpdateTraSach(sach);
        }
        public bool DeleteTraSachh(MuonSach sach)
        {
            return cnms_dal.DeleteTraSachh(sach);
        }


    }
}
