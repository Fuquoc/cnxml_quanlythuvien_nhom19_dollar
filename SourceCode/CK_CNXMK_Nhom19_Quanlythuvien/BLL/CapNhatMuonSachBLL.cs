﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;

namespace CK_CNXMK_Nhom19_Quanlythuvien.BLL
{
    class CapNhatMuonSachBLL
    {
        CapNhatMuonSachDAL cnms_dal;

        public CapNhatMuonSachBLL()
        {
            cnms_dal = new CapNhatMuonSachDAL();
        }

        public DataTable getMuonSach()
        {
            return cnms_dal.getMuonSach();
        }

        public DataTable XuatMaSach()
        {
            return cnms_dal.XuatMaSach();
        }
        public DataTable XuatTenSach()
        {
            return cnms_dal.XuatTenSach();
        }
      
        
        public DataTable XuatDG()
        {
            return cnms_dal.XuatDG();
        }

        public bool InsertMuonSach(MuonSach sach)
        {
            return cnms_dal.InsertMuonSach(sach);
        }
        public bool UpdateMuonSach(MuonSach sach)
        {
            return cnms_dal.UpdateMuonSach(sach);
        }
        public bool DeleteMuonSach(MuonSach sach)
        {
            return cnms_dal.DeleteMuonSach(sach);
        }
    }
}
