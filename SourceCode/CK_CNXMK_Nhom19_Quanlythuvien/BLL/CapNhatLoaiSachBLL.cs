﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;


namespace CK_CNXMK_Nhom19_Quanlythuvien.BLL
{
    
    class CapNhatLoaiSachBLL
    {
        CapNhatLoaiSachDAL cnls_dal;
        public  CapNhatLoaiSachBLL()
        {
            cnls_dal = new CapNhatLoaiSachDAL();
        }

        public DataTable getLoaiSach()
        {
            return cnls_dal.getLoaiSach();
        }
        public bool InserttblLoaiSach(LoaiSach loaisach)
        {
            return cnls_dal.InserttblLoaiSach(loaisach);
        }
        public bool UpdatetblLoaiSach(LoaiSach loaisach)
        {
            return cnls_dal.UpdatetblLoaiSach(loaisach);
        }
        public bool DELETEtblLoaiSach(LoaiSach loaisach)
        {
            return cnls_dal.DELETEtblLoaiSach(loaisach);
        }
        public DataTable FindtblLoaiSach(string loaisach)
        {
            return cnls_dal.FindtblLoaiSach(loaisach);
        }


    }
}
