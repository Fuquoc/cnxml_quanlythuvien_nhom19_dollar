﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;

namespace CK_CNXMK_Nhom19_Quanlythuvien.BLL
{
    class QuanLiDocGiaBLL
    {
        QuanLiDocGiaDAL qldg_dal;
        public QuanLiDocGiaBLL()
        {
            qldg_dal = new QuanLiDocGiaDAL();
        }

         public DataTable getDocGia()
        {
            return qldg_dal.getDocGia();
        }
        public bool InsertDocGia(DocGia dg)
        {
            return qldg_dal.InsertDocGia(dg);
        }
        public bool UpdateDocGIa(DocGia dg)
        {
            return qldg_dal.UpdateDocGIa(dg);
        }
        public bool DeleteDocGia (DocGia dg)
        {
            return qldg_dal.DeleteDocGia(dg);
        }

        

        public DataTable FindDocGia(string dg)
        {
            return qldg_dal.FindDocGia(dg);
        }
    }

   
}
