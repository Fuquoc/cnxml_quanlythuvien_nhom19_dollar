﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    class CapNhatSachBLL
    {
        CapNhatSachDAL cns_dal;
        public CapNhatSachBLL()
        {
            cns_dal = new CapNhatSachDAL();

        }
        public DataTable getALLSach()
        {
            return cns_dal.getALLSach();
        }

        public DataTable XuatTG()
        {
            return cns_dal.XuatTG();
        }

        public DataTable XuatMaLoaiSach()
        {
            return cns_dal.XuatMaLoaiSach();
        }
        public bool InserttblSach(Sach sach)
        {
            return cns_dal.InserttblSach(sach);
        }
        public bool UpdatetblSach(Sach sach)
        {
            return cns_dal.UpdatetblSach(sach);
        }
        public bool DELETEtblSach(Sach sach)
        {
            return cns_dal.DELETEtblSach(sach);
        }
        public DataTable Find_TenSach(string sach)
        {
            return cns_dal.Find_TenSach(sach);
        }

        public DataTable Find_MaSach(string sach)
        {
            return cns_dal.Find_MaSach(sach);
        }

        public DataTable Find_MaLoaiSach(string sach)
        {
            return cns_dal.Find_MaLoaiSach(sach);
        }

        public DataTable Find_MaTacGia(string sach)
        {
            return cns_dal.Find_MaTacGia(sach);
        }
    }
}
