﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class DoiMatKhau : Form
    {
        DangNhapHT dn = new DangNhapHT();
        DoiMK dmk = new DoiMK();
        public DoiMatKhau()
        {
            InitializeComponent();
        }

        bool LoadDuLieu()
        {

            if (!dmk.KiemTraMK(txtMatKhauCu.Text))
            {
                MessageBox.Show("Sai mật khẩu cũ");
                return false;
            }
            if (!txtMatKhauMoi.Text.Equals(txtNhapLai.Text))
            {
                MessageBox.Show("Mật khẩu mới sai");
                return false;
            }
            return true;
        }
        public void DoiMatKhauMoi(string nguoiDung, string matKhau)
        {
            XmlDocument doc1 = new XmlDocument();
            doc1.Load(Application.StartupPath + "\\taiKhoan.xml");
            XmlNode node1 = doc1.SelectSingleNode("NewDataSet/TaiKhoan[Id = '" + nguoiDung + "']");
            if (node1 != null)
            {
                node1.ChildNodes[1].InnerText = matKhau;
                doc1.Save(Application.StartupPath + "\\taiKhoan.xml");
            }
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            if (LoadDuLieu())
            {
                if (MessageBox.Show("Bạn có chắc muốn đổi mật khẩu không?", "Thông Báo!", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    if (dn.kiemtraMK("taiKhoan.xml", txtMatKhauCu.Text) == true)
                    {
                        dmk.Doi(txtMatKhauMoi.Text);
                        MessageBox.Show("Đổi mật khẩu thành công");
                        HeThong_ThuVien ht = new HeThong_ThuVien();
                        ht.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Mật khẩu cũ không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtMatKhauCu.Text = "";
                        txtMatKhauMoi.Text = "";
                        txtNhapLai.Text = "";
                    }
                      
                }
            }

        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }

        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            txtMatKhauCu.Text = "";
            txtMatKhauMoi.Text = "";
            txtNhapLai.Text = "";
        }

        private void DoiMatKhau_Load(object sender, EventArgs e)
        {

        }
    }
}
