﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien.BLL;
/*using XML_CUOIKI.Get_Set;*/

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class XemDanhSach : Form
    {
        int index;
        CapNhatLoaiSachBLL cnls;
        QuanLiTacGiaBLL qltg;
        CapNhatTraSachBLL cnts;
        public XemDanhSach(int index)
        {
            this.index = index;
            InitializeComponent();
            cnls = new CapNhatLoaiSachBLL();
            qltg = new QuanLiTacGiaBLL();
            cnts = new CapNhatTraSachBLL();
        }

        public void ShowLoaiSach()
        {
            DataTable dt1 = cnls.getLoaiSach();
            DataGridView1.DataSource = dt1;

        }
        public void ShowTacGia()
        {
            DataTable dt1 = qltg.getTacGia();
            DataGridView1.DataSource = dt1;
        }
        public void ShowTraSach()
        {
            DataTable dt1 = cnts.getTraSach();
            DataGridView1.DataSource = dt1;


        }


        private void CapNhatLoaiSach_Load(object sender, EventArgs e)
        {
            switch (index)
            {
                case 0:
                    LabelTitle_XemDanhSach.Text = "Danh Sách Loại Sách";
                    ShowLoaiSach();
                    break;
                case 1:
                    LabelTitle_XemDanhSach.Text = "Danh Sách Tác Giả";
                    ShowTacGia();
                    break;
                case 2:
                    LabelTitle_XemDanhSach.Text = "Danh Sách Độc Giả Mượn và Trả Sách";
                    ShowTraSach();
                    break;
            }
        }

        

        private void lb_dsls_Click(object sender, EventArgs e)
        {

        }
    }
}
