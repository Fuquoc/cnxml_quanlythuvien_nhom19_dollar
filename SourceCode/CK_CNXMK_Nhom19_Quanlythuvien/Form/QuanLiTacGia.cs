﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien.BLL;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class QuanLiTacGia : Form
    {
        QuanLiTacGiaBLL qltg_bll;
        public QuanLiTacGia()
        {
            InitializeComponent();
            qltg_bll = new QuanLiTacGiaBLL();
        }

        public void Show_TacGia()
        {
            DataTable dt = qltg_bll.getTacGia();
            DataGridView1.DataSource = dt;

        }

       

        public bool CheckData()
        {
            if (string.IsNullOrEmpty(tb_matg.Text))

            {
                MessageBox.Show("Vui lòng nhập dữ liệu! ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                tb_matg.Focus();
                return false;
            }
            return true;
        }
        private void bt_save_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn lưu không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {
                    TacGia tg = new TacGia();
                    tg.MaTG = tb_matg.Text;
                    tg.TenTG = tb_tentg.Text;                 
                    tg.DiaChi = tb_diachi.Text;
                    if (qltg_bll.InsertTacGia(tg))
                    {
                        MessageBox.Show("Thêm thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Show_TacGia();
                    }

                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            bt_them.Enabled = true;
            bt_save.Enabled = false;
            bt_sua.Enabled = true;
            bt_xoa.Enabled = true;
            bt_thoat.Enabled = true;
        }

        private void QuanLiTacGia_Load(object sender, EventArgs e)
        {
            Show_TacGia();
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                tb_matg.Enabled = true;
                tb_matg.Text = DataGridView1.Rows[index].Cells["MaTG"].Value.ToString();
                tb_tentg.Text = DataGridView1.Rows[index].Cells["TenTG"].Value.ToString();
                tb_diachi.Text = DataGridView1.Rows[index].Cells["DiaChi"].Value.ToString();


            }
        }

        private void bt_thoat_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }

        }

        

        private void bt_sua_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn sửa không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {

                    TacGia tg = new TacGia();
                    tb_matg.Enabled = false;
                    tg.MaTG = tb_matg.Text;
                    tg.TenTG = tb_tentg.Text;
                    tg.DiaChi = tb_diachi.Text;

                    if (qltg_bll.UpdateTacGIa(tg))
                    {


                        MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Show_TacGia();
                    }
                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }
        }

        private void bt_them_Click(object sender, EventArgs e)
        {

            tb_matg.Enabled = true;
            tb_matg.ResetText();
            tb_tentg.ResetText();
            tb_diachi.ResetText();

            bt_them.Enabled = false;
            bt_save.Enabled = true;
            bt_sua.Enabled = false;
            bt_xoa.Enabled = false;
            bt_thoat.Enabled = true;
        }

        private void bt_xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa hay không??", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                TacGia tg = new TacGia();
                tg.MaTG = tb_matg.Text;

                if (qltg_bll.DeleteTacGia(tg))
                {
                    MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Show_TacGia();
                }
                else
                    MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            tb_matg.Text = "";
            tb_tentg.Text = "";
            tb_diachi.Text = "";
        }
    }
}
