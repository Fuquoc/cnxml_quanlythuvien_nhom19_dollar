﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class HeThong_ThuVien : Form
    {
        public static string tenDNMain = "";
        public  string maNVMain = "";
        FileXML Fxml = new FileXML();
        HeThong HT = new HeThong();
        public HeThong_ThuVien()
        {
            InitializeComponent();
        }
        public HeThong_ThuVien(string a) : this()
        {

            tenDNMain = a;
        }

        
        private bool CheckForm(string nameForm)
        {
            bool check = false;
            foreach (Form frm in this.MdiChildren)
                if (frm.Name == nameForm)
                {
                    check = true;
                    break;
                }
            return check;

        }
        private void ActivateForm(string nameForm)
        {
            foreach (Form frm in this.MdiChildren)
                if (frm.Name == nameForm)
                {
                    frm.Activate();
                    break;
                }
        }


       
        
        private void đăngNhậpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!CheckForm("DangNhap"))
            {
                Form frm = new DangNhap();
                frm.MdiParent = this;
                frm.Show();
            }
            else
            {
                ActivateForm("DangNhap");
            }
            //đăngNhậpToolStripMenuItem.Enabled = false;
        }

        private void cậpNhậtTácGiảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            QuanLiTacGia tg = new QuanLiTacGia();
            tg.Show();
            this.Hide();
        }

        

       
        private void thoátToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void đăngXuấtToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DangNhap dn = new DangNhap();
            dn.Show();
            this.Hide();
        }

        private void đổiMậtKhẩuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DoiMatKhau dmk = new DoiMatKhau();
            dmk.Show();
            this.Hide();
        }

        private void sQLSangXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            try
            {
                HT.TaoXML();
                MessageBox.Show("Tạo XML thành công" , "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("" + ex);
            }
        }

        private void nhânViênToolStripMenuItem_Click(object sender, EventArgs e)
        {
            taoTaiKhoan tk = new taoTaiKhoan();
            tk.Show();
            this.Hide();
        }

        private void quảnLýTàiKhoảnToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();

            taoTaiKhoan tk = new taoTaiKhoan();
            tk.TopLevel = false;
            tk.TopMost = true;
            panel1.Controls.Add(tk);
            tk.Show();
        }

        private void quảnLýSáchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();

            CapNhat_Sach dt = new CapNhat_Sach();
            dt.TopLevel = false;
            dt.TopMost = true;
            panel1.Controls.Add(dt);
            dt.Show();
        }
        private void quảnLýLoạiSáchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();
            XemDanhSach dt = new XemDanhSach(0);
            dt.TopLevel = false;
            dt.TopMost = true;
            panel1.Controls.Add(dt);
            dt.Show();
        }

        private void quảnLýTácGiảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();

            XemDanhSach dt = new XemDanhSach(1);
            dt.TopLevel = false;
            dt.TopMost = true;
            panel1.Controls.Add(dt);
            dt.Show();
        }

        private void xemDanhSáchMượnTrảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();

            XemDanhSach dt = new XemDanhSach(2);
            dt.TopLevel = false;
            dt.TopMost = true;
            panel1.Controls.Add(dt);
            dt.Show();
        }

        private void quảnLýĐộcGiảToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panel1.Controls.Clear();

            QuanLiDocGia dt = new QuanLiDocGia();
            dt.TopLevel = false;
            dt.TopMost = true;
            panel1.Controls.Add(dt);
            dt.Show();
        }

        private void chuyểnĐỗiDữLiệuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
