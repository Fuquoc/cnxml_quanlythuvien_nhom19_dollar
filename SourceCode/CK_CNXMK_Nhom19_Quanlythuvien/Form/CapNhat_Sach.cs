﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class CapNhat_Sach : Form
    {
        CapNhatSachBLL cnsbll;
        public CapNhat_Sach()
        {
            InitializeComponent();
            cnsbll = new CapNhatSachBLL();
        }

      

       public void ShowALLSach ()
        {
            DataTable dt = cnsbll.getALLSach();
            DataGridView1.DataSource = dt;
           
        }

        public  void ShowTG()
        {
            DataTable dt = cnsbll.XuatTG();
            cb_maTG.DataSource = dt;
            cb_maTG.ValueMember = "MaTG";
        }

        public void ShowMaLoaiSach()
        {
            DataTable dt = cnsbll.XuatMaLoaiSach();
            cb_maloai.DataSource = dt;
            cb_maloai.ValueMember = "MaLoaiSach";
        }
        private void CapNhat_Sach_Load(object sender, EventArgs e)
        {
            ShowALLSach();
            ShowTG();
            ShowMaLoaiSach();
        }

        public bool CheckData()
        {
            if (string.IsNullOrEmpty(tb_masach.Text))

            {
                MessageBox.Show("Vui lòng nhập dữ liệu ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                tb_masach.Focus();
                return false;
            }
            return true;
        }
        private void bt_them_Click(object sender, EventArgs e)
           
        {
            tb_masach.Enabled = true;
            tb_masach.Text = "";
            tb_tensach.Text = "";
            cb_maloai.Text = "";
            cb_maTG.Text = "";
            tb_soluong.Text = "";
            bt_them.Enabled = false;
            bt_luu.Enabled = true;
            bt_sua.Enabled = false;
            bt_xoa.Enabled = false;
        }
     
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                tb_masach.Text = DataGridView1.Rows[index].Cells["MaSach"].Value.ToString();
                tb_tensach.Text = DataGridView1.Rows[index].Cells["TenSach"].Value.ToString();
                cb_maloai.Text = DataGridView1.Rows[index].Cells["MaLoaiSach"].Value.ToString();
                cb_maTG.Text = DataGridView1.Rows[index].Cells["MaTG"].Value.ToString();
                tb_soluong.Text = DataGridView1.Rows[index].Cells["SoLuong"].Value.ToString();
            }  
        }

        private void bt_thoat_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }
        }

        private void bt_luu_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn lưu không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {
                    Sach s = new Sach();
                    s.MaSach = tb_masach.Text;
                    s.TenSach = tb_tensach.Text;
                    s.MaLoaiSach = cb_maloai.Text;
                    s.MaTG = cb_maTG.Text;
                    s.SoLuong = int.Parse(tb_soluong.Text);
                    if (cnsbll.InserttblSach(s))
                    {
                        
                        ShowALLSach();
                        MessageBox.Show("Thêm thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }
            bt_them.Enabled = true;
            bt_luu.Enabled = false;
            bt_sua.Enabled = true;
            bt_xoa.Enabled = true;
        }

        private void bt_sua_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn sửa không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {
                    Sach sach = new Sach();
                    sach.MaSach = tb_masach.Text;
                    sach.TenSach = tb_tensach.Text;
                    sach.MaLoaiSach = cb_maloai.Text;
                    sach.MaTG = cb_maTG.Text;
                    sach.SoLuong = int.Parse(tb_soluong.Text);
                    if (cnsbll.UpdatetblSach(sach))
                    {
                        ShowALLSach();
                        MessageBox.Show("Sửa thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }

        }

        private void bt_xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("bạn có muốn xóa hay không??", "thông báo lỗi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                Sach sach = new Sach();
                sach.MaSach = tb_masach.Text;

                if (cnsbll.DELETEtblSach(sach))
                {
                    ShowALLSach();
                    MessageBox.Show("Xóa thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                    
                else
                    MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            tb_masach.Text = "";
            tb_tensach.Text = "";
            cb_maloai.Text = "";
            cb_maTG.Text = "";
            tb_soluong.Text = "";
        }

        private void LB_CTS_Click(object sender, EventArgs e)
        {

        }
    }
}
