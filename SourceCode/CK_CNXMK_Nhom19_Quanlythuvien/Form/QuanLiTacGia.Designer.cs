﻿
namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class QuanLiTacGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.Label5 = new System.Windows.Forms.Label();
            this.bt_thoat = new System.Windows.Forms.Button();
            this.bt_xoa = new System.Windows.Forms.Button();
            this.bt_sua = new System.Windows.Forms.Button();
            this.bt_save = new System.Windows.Forms.Button();
            this.bt_them = new System.Windows.Forms.Button();
            this.tb_diachi = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.tb_tentg = new System.Windows.Forms.TextBox();
            this.tb_matg = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // DataGridView1
            // 
            this.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(40, 333);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.DataGridView1.MultiSelect = false;
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowHeadersWidth = 51;
            this.DataGridView1.RowTemplate.Height = 24;
            this.DataGridView1.Size = new System.Drawing.Size(617, 191);
            this.DataGridView1.TabIndex = 30;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.Location = new System.Drawing.Point(250, 295);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(246, 32);
            this.Label5.TabIndex = 28;
            this.Label5.Text = "Danh Sách Tác Giả";
            // 
            // bt_thoat
            // 
            this.bt_thoat.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_thoat.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.thoat;
            this.bt_thoat.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_thoat.Location = new System.Drawing.Point(530, 224);
            this.bt_thoat.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bt_thoat.Name = "bt_thoat";
            this.bt_thoat.Size = new System.Drawing.Size(82, 37);
            this.bt_thoat.TabIndex = 27;
            this.bt_thoat.Text = "Thoát";
            this.bt_thoat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_thoat.UseVisualStyleBackColor = true;
            this.bt_thoat.Click += new System.EventHandler(this.bt_thoat_Click);
            // 
            // bt_xoa
            // 
            this.bt_xoa.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_xoa.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.xoa;
            this.bt_xoa.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_xoa.Location = new System.Drawing.Point(436, 224);
            this.bt_xoa.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bt_xoa.Name = "bt_xoa";
            this.bt_xoa.Size = new System.Drawing.Size(72, 37);
            this.bt_xoa.TabIndex = 26;
            this.bt_xoa.Text = "Xóa";
            this.bt_xoa.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_xoa.UseVisualStyleBackColor = true;
            this.bt_xoa.Click += new System.EventHandler(this.bt_xoa_Click);
            // 
            // bt_sua
            // 
            this.bt_sua.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_sua.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.sua;
            this.bt_sua.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_sua.Location = new System.Drawing.Point(310, 224);
            this.bt_sua.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bt_sua.Name = "bt_sua";
            this.bt_sua.Size = new System.Drawing.Size(79, 37);
            this.bt_sua.TabIndex = 25;
            this.bt_sua.Text = "Sửa";
            this.bt_sua.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_sua.UseVisualStyleBackColor = true;
            this.bt_sua.Click += new System.EventHandler(this.bt_sua_Click);
            // 
            // bt_save
            // 
            this.bt_save.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_save.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.save;
            this.bt_save.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_save.Location = new System.Drawing.Point(192, 224);
            this.bt_save.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(74, 37);
            this.bt_save.TabIndex = 24;
            this.bt_save.Text = "Lưu";
            this.bt_save.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_save.UseVisualStyleBackColor = true;
            this.bt_save.Click += new System.EventHandler(this.bt_save_Click);
            // 
            // bt_them
            // 
            this.bt_them.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_them.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.add1;
            this.bt_them.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_them.Location = new System.Drawing.Point(64, 224);
            this.bt_them.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.bt_them.Name = "bt_them";
            this.bt_them.Size = new System.Drawing.Size(85, 37);
            this.bt_them.TabIndex = 23;
            this.bt_them.Text = "Thêm";
            this.bt_them.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_them.UseVisualStyleBackColor = true;
            this.bt_them.Click += new System.EventHandler(this.bt_them_Click);
            // 
            // tb_diachi
            // 
            this.tb_diachi.Location = new System.Drawing.Point(468, 94);
            this.tb_diachi.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tb_diachi.Name = "tb_diachi";
            this.tb_diachi.Size = new System.Drawing.Size(198, 25);
            this.tb_diachi.TabIndex = 22;
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.Location = new System.Drawing.Point(390, 96);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(73, 21);
            this.Label4.TabIndex = 21;
            this.Label4.Text = "Địa Chỉ:";
            // 
            // tb_tentg
            // 
            this.tb_tentg.Location = new System.Drawing.Point(142, 153);
            this.tb_tentg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tb_tentg.Name = "tb_tentg";
            this.tb_tentg.Size = new System.Drawing.Size(195, 25);
            this.tb_tentg.TabIndex = 20;
            // 
            // tb_matg
            // 
            this.tb_matg.Location = new System.Drawing.Point(142, 98);
            this.tb_matg.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tb_matg.Name = "tb_matg";
            this.tb_matg.Size = new System.Drawing.Size(195, 25);
            this.tb_matg.TabIndex = 19;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.Location = new System.Drawing.Point(24, 153);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(108, 21);
            this.Label3.TabIndex = 18;
            this.Label3.Text = "Tên Tác Giả:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Times New Roman", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(27, 100);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(103, 21);
            this.Label2.TabIndex = 17;
            this.Label2.Text = "Mã Tác Giả:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(284, 36);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(211, 32);
            this.Label1.TabIndex = 16;
            this.Label1.Text = "Chi Tiết Tác Giả";
            // 
            // QuanLiTacGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(689, 537);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.bt_thoat);
            this.Controls.Add(this.bt_xoa);
            this.Controls.Add(this.bt_sua);
            this.Controls.Add(this.bt_save);
            this.Controls.Add(this.bt_them);
            this.Controls.Add(this.tb_diachi);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.tb_tentg);
            this.Controls.Add(this.tb_matg);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "QuanLiTacGia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập Nhật Tác Giả";
            this.Load += new System.EventHandler(this.QuanLiTacGia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Button bt_thoat;
        internal System.Windows.Forms.Button bt_xoa;
        internal System.Windows.Forms.Button bt_sua;
        internal System.Windows.Forms.Button bt_save;
        internal System.Windows.Forms.Button bt_them;
        internal System.Windows.Forms.TextBox tb_diachi;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.TextBox tb_tentg;
        internal System.Windows.Forms.TextBox tb_matg;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}