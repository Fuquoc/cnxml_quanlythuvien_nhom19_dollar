﻿
namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class QuanLiDocGia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.Label6 = new System.Windows.Forms.Label();
            this.tb_lop = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.radio_Nu = new System.Windows.Forms.RadioButton();
            this.radio_Nam = new System.Windows.Forms.RadioButton();
            this.gb_gioitinh = new System.Windows.Forms.GroupBox();
            this.tb_tendg = new System.Windows.Forms.TextBox();
            this.tb_madg = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.bt_xoa = new System.Windows.Forms.Button();
            this.bt_sua = new System.Windows.Forms.Button();
            this.bt_Save = new System.Windows.Forms.Button();
            this.bt_them = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.gb_gioitinh.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataGridView1
            // 
            this.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(140, 335);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowHeadersWidth = 100;
            this.DataGridView1.RowTemplate.Height = 24;
            this.DataGridView1.Size = new System.Drawing.Size(700, 191);
            this.DataGridView1.TabIndex = 34;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(373, 300);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(249, 32);
            this.Label6.TabIndex = 32;
            this.Label6.Text = "Danh Sách Độc Giả";
            // 
            // tb_lop
            // 
            this.tb_lop.Location = new System.Drawing.Point(457, 203);
            this.tb_lop.Name = "tb_lop";
            this.tb_lop.Size = new System.Drawing.Size(203, 27);
            this.tb_lop.TabIndex = 26;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(347, 203);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(41, 19);
            this.Label5.TabIndex = 25;
            this.Label5.Text = "Lớp:";
            // 
            // radio_Nu
            // 
            this.radio_Nu.AutoSize = true;
            this.radio_Nu.Location = new System.Drawing.Point(79, 32);
            this.radio_Nu.Name = "radio_Nu";
            this.radio_Nu.Size = new System.Drawing.Size(51, 23);
            this.radio_Nu.TabIndex = 1;
            this.radio_Nu.TabStop = true;
            this.radio_Nu.Text = "Nữ";
            this.radio_Nu.UseVisualStyleBackColor = true;
            // 
            // radio_Nam
            // 
            this.radio_Nam.AutoSize = true;
            this.radio_Nam.Location = new System.Drawing.Point(7, 32);
            this.radio_Nam.Name = "radio_Nam";
            this.radio_Nam.Size = new System.Drawing.Size(62, 23);
            this.radio_Nam.TabIndex = 0;
            this.radio_Nam.TabStop = true;
            this.radio_Nam.Text = "Nam";
            this.radio_Nam.UseVisualStyleBackColor = true;
            // 
            // gb_gioitinh
            // 
            this.gb_gioitinh.Controls.Add(this.radio_Nu);
            this.gb_gioitinh.Controls.Add(this.radio_Nam);
            this.gb_gioitinh.Location = new System.Drawing.Point(698, 120);
            this.gb_gioitinh.Name = "gb_gioitinh";
            this.gb_gioitinh.Size = new System.Drawing.Size(132, 73);
            this.gb_gioitinh.TabIndex = 24;
            this.gb_gioitinh.TabStop = false;
            this.gb_gioitinh.Text = "Giới Tính";
            // 
            // tb_tendg
            // 
            this.tb_tendg.Location = new System.Drawing.Point(457, 171);
            this.tb_tendg.Name = "tb_tendg";
            this.tb_tendg.Size = new System.Drawing.Size(204, 27);
            this.tb_tendg.TabIndex = 23;
            // 
            // tb_madg
            // 
            this.tb_madg.Location = new System.Drawing.Point(457, 138);
            this.tb_madg.Name = "tb_madg";
            this.tb_madg.Size = new System.Drawing.Size(204, 27);
            this.tb_madg.TabIndex = 22;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(347, 174);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(100, 19);
            this.Label3.TabIndex = 21;
            this.Label3.Text = "Tên Độc Giả:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(347, 141);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(97, 19);
            this.Label2.TabIndex = 20;
            this.Label2.Text = "Mã Độc Giả:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.Label1.Font = new System.Drawing.Font("Times New Roman", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(25, 9);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(262, 38);
            this.Label1.TabIndex = 19;
            this.Label1.Text = "Chi Tiết Độc Giả";
            // 
            // bt_xoa
            // 
            this.bt_xoa.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_xoa.ForeColor = System.Drawing.Color.Blue;
            this.bt_xoa.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.xoa;
            this.bt_xoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_xoa.Location = new System.Drawing.Point(348, 248);
            this.bt_xoa.Name = "bt_xoa";
            this.bt_xoa.Size = new System.Drawing.Size(93, 49);
            this.bt_xoa.TabIndex = 30;
            this.bt_xoa.Text = "Xóa";
            this.bt_xoa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_xoa.UseVisualStyleBackColor = true;
            this.bt_xoa.Click += new System.EventHandler(this.bt_xoa_Click);
            // 
            // bt_sua
            // 
            this.bt_sua.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_sua.ForeColor = System.Drawing.Color.Blue;
            this.bt_sua.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.sua;
            this.bt_sua.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_sua.Location = new System.Drawing.Point(162, 211);
            this.bt_sua.Name = "bt_sua";
            this.bt_sua.Size = new System.Drawing.Size(95, 52);
            this.bt_sua.TabIndex = 29;
            this.bt_sua.Text = "Sửa";
            this.bt_sua.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_sua.UseVisualStyleBackColor = true;
            this.bt_sua.Click += new System.EventHandler(this.bt_sua_Click);
            // 
            // bt_Save
            // 
            this.bt_Save.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_Save.ForeColor = System.Drawing.Color.Blue;
            this.bt_Save.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.save;
            this.bt_Save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_Save.Location = new System.Drawing.Point(508, 248);
            this.bt_Save.Name = "bt_Save";
            this.bt_Save.Size = new System.Drawing.Size(89, 49);
            this.bt_Save.TabIndex = 28;
            this.bt_Save.Text = "Lưu";
            this.bt_Save.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_Save.UseVisualStyleBackColor = true;
            this.bt_Save.Click += new System.EventHandler(this.bt_Save_Click);
            // 
            // bt_them
            // 
            this.bt_them.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_them.ForeColor = System.Drawing.Color.Blue;
            this.bt_them.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.add1;
            this.bt_them.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_them.Location = new System.Drawing.Point(162, 97);
            this.bt_them.Name = "bt_them";
            this.bt_them.Size = new System.Drawing.Size(95, 52);
            this.bt_them.TabIndex = 27;
            this.bt_them.Text = "Thêm";
            this.bt_them.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_them.UseVisualStyleBackColor = true;
            this.bt_them.Click += new System.EventHandler(this.bt_them_Click);
            // 
            // QuanLiDocGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Azure;
            this.BackgroundImage = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.backgroundqlidocgia;
            this.ClientSize = new System.Drawing.Size(939, 538);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.bt_xoa);
            this.Controls.Add(this.bt_sua);
            this.Controls.Add(this.bt_Save);
            this.Controls.Add(this.bt_them);
            this.Controls.Add(this.tb_lop);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.gb_gioitinh);
            this.Controls.Add(this.tb_tendg);
            this.Controls.Add(this.tb_madg);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "QuanLiDocGia";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập Nhật Độc Giả";
            this.Load += new System.EventHandler(this.QuanLiDocGia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.gb_gioitinh.ResumeLayout(false);
            this.gb_gioitinh.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Button bt_xoa;
        internal System.Windows.Forms.Button bt_sua;
        internal System.Windows.Forms.Button bt_Save;
        internal System.Windows.Forms.Button bt_them;
        internal System.Windows.Forms.TextBox tb_lop;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.RadioButton radio_Nu;
        internal System.Windows.Forms.RadioButton radio_Nam;
        internal System.Windows.Forms.GroupBox gb_gioitinh;
        internal System.Windows.Forms.TextBox tb_tendg;
        internal System.Windows.Forms.TextBox tb_madg;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
    }
}