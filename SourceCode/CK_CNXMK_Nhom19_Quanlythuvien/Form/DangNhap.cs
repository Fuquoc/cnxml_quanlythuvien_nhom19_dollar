﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class DangNhap : Form
    {
        FileXML Fxml = new FileXML();
        DangNhapHT dn = new DangNhapHT();
        

        public DangNhap()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (tb_tenDN.Text.Equals("") || tb_pass.Text.Equals(""))
            {
                MessageBox.Show("Không được bỏ trống các trường!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                tb_tenDN.Focus();
            }
            else
            {

                //if (dn.kiemtraTTDN(HeThong.BangTaiKhoan, tb_tenDN.Text, tb_pass.Text) == true)
                if(dn.kiemtraDangNhap(tb_tenDN.Text,tb_pass.Text))
                {
                    MessageBox.Show("Đăng nhập thành công");
                
                    HeThong_ThuVien frm = new HeThong_ThuVien(tb_tenDN.Text);
                    frm.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Tên đăng nhập hoặc mật khẩu không đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    tb_tenDN.Text = "";
                    tb_pass.Text = "";
                    tb_tenDN.Focus();
                }
            }
        }

        //private void bt_thoat_Click(object sender, EventArgs e)
        //{
        //    this.Close();
        //}

        private void DangNhap_Load(object sender, EventArgs e)
        {

        }

        private void tb_tenDN_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
