﻿
namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class CapNhatMuonSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapNhatMuonSach));
            this.Label11 = new System.Windows.Forms.Label();
            this.cb_masach = new System.Windows.Forms.ComboBox();
            this.date_nghen = new System.Windows.Forms.DateTimePicker();
            this.date_ngmuon = new System.Windows.Forms.DateTimePicker();
            this.tb_sl_muon = new System.Windows.Forms.TextBox();
            this.cb_madg = new System.Windows.Forms.ComboBox();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.Label10 = new System.Windows.Forms.Label();
            this.Label9 = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.Label7 = new System.Windows.Forms.Label();
            this.Label6 = new System.Windows.Forms.Label();
            this.bt_Xoa = new System.Windows.Forms.Button();
            this.bt_sua = new System.Windows.Forms.Button();
            this.bt_ketthuc = new System.Windows.Forms.Button();
            this.bt_chomuon = new System.Windows.Forms.Button();
            this.bt_muonmoi = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.GroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(542, 50);
            this.Label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(275, 31);
            this.Label11.TabIndex = 21;
            this.Label11.Text = "Danh Sách Cho Mượn";
            // 
            // cb_masach
            // 
            this.cb_masach.FormattingEnabled = true;
            this.cb_masach.Location = new System.Drawing.Point(138, 64);
            this.cb_masach.Margin = new System.Windows.Forms.Padding(2);
            this.cb_masach.Name = "cb_masach";
            this.cb_masach.Size = new System.Drawing.Size(132, 21);
            this.cb_masach.TabIndex = 10;
            // 
            // date_nghen
            // 
            this.date_nghen.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_nghen.Location = new System.Drawing.Point(138, 190);
            this.date_nghen.Margin = new System.Windows.Forms.Padding(2);
            this.date_nghen.Name = "date_nghen";
            this.date_nghen.Size = new System.Drawing.Size(132, 20);
            this.date_nghen.TabIndex = 9;
            // 
            // date_ngmuon
            // 
            this.date_ngmuon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.date_ngmuon.Location = new System.Drawing.Point(138, 144);
            this.date_ngmuon.Margin = new System.Windows.Forms.Padding(2);
            this.date_ngmuon.Name = "date_ngmuon";
            this.date_ngmuon.Size = new System.Drawing.Size(132, 20);
            this.date_ngmuon.TabIndex = 8;
            // 
            // tb_sl_muon
            // 
            this.tb_sl_muon.Location = new System.Drawing.Point(138, 104);
            this.tb_sl_muon.Margin = new System.Windows.Forms.Padding(2);
            this.tb_sl_muon.Name = "tb_sl_muon";
            this.tb_sl_muon.Size = new System.Drawing.Size(132, 20);
            this.tb_sl_muon.TabIndex = 7;
            this.tb_sl_muon.Text = " ";
            // 
            // cb_madg
            // 
            this.cb_madg.FormattingEnabled = true;
            this.cb_madg.Items.AddRange(new object[] {
            "1811505310235",
            "1811505310236",
            "1811505310237",
            "1811505310238"});
            this.cb_madg.Location = new System.Drawing.Point(138, 24);
            this.cb_madg.Margin = new System.Windows.Forms.Padding(2);
            this.cb_madg.Name = "cb_madg";
            this.cb_madg.Size = new System.Drawing.Size(132, 21);
            this.cb_madg.TabIndex = 5;
            // 
            // DataGridView1
            // 
            this.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(443, 99);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowHeadersWidth = 51;
            this.DataGridView1.Size = new System.Drawing.Size(483, 147);
            this.DataGridView1.TabIndex = 23;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Location = new System.Drawing.Point(13, 197);
            this.Label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(77, 13);
            this.Label10.TabIndex = 4;
            this.Label10.Text = "Ngày Hẹn Trả:";
            // 
            // Label9
            // 
            this.Label9.AutoSize = true;
            this.Label9.Location = new System.Drawing.Point(13, 150);
            this.Label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(65, 13);
            this.Label9.TabIndex = 3;
            this.Label9.Text = "Ngày Mượn:";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.cb_masach);
            this.GroupBox2.Controls.Add(this.date_nghen);
            this.GroupBox2.Controls.Add(this.date_ngmuon);
            this.GroupBox2.Controls.Add(this.tb_sl_muon);
            this.GroupBox2.Controls.Add(this.cb_madg);
            this.GroupBox2.Controls.Add(this.Label10);
            this.GroupBox2.Controls.Add(this.Label9);
            this.GroupBox2.Controls.Add(this.Label8);
            this.GroupBox2.Controls.Add(this.Label7);
            this.GroupBox2.Controls.Add(this.Label6);
            this.GroupBox2.Location = new System.Drawing.Point(75, 50);
            this.GroupBox2.Margin = new System.Windows.Forms.Padding(2);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Padding = new System.Windows.Forms.Padding(2);
            this.GroupBox2.Size = new System.Drawing.Size(284, 297);
            this.GroupBox2.TabIndex = 17;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Thực Hiện Cho Mượn";
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(13, 109);
            this.Label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(86, 13);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "Số Lượng Mượn:";
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(13, 68);
            this.Label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(53, 13);
            this.Label7.TabIndex = 1;
            this.Label7.Text = "Mã Sách:";
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(13, 27);
            this.Label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(67, 13);
            this.Label6.TabIndex = 0;
            this.Label6.Text = "Mã Độc Giả:";
            // 
            // bt_Xoa
            // 
            this.bt_Xoa.Image = ((System.Drawing.Image)(resources.GetObject("bt_Xoa.Image")));
            this.bt_Xoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_Xoa.Location = new System.Drawing.Point(652, 305);
            this.bt_Xoa.Margin = new System.Windows.Forms.Padding(2);
            this.bt_Xoa.Name = "bt_Xoa";
            this.bt_Xoa.Size = new System.Drawing.Size(103, 43);
            this.bt_Xoa.TabIndex = 25;
            this.bt_Xoa.Text = "Xóa";
            this.bt_Xoa.UseVisualStyleBackColor = true;
            this.bt_Xoa.Click += new System.EventHandler(this.bt_Xoa_Click);
            // 
            // bt_sua
            // 
            this.bt_sua.Image = ((System.Drawing.Image)(resources.GetObject("bt_sua.Image")));
            this.bt_sua.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_sua.Location = new System.Drawing.Point(797, 305);
            this.bt_sua.Margin = new System.Windows.Forms.Padding(2);
            this.bt_sua.Name = "bt_sua";
            this.bt_sua.Size = new System.Drawing.Size(97, 43);
            this.bt_sua.TabIndex = 24;
            this.bt_sua.Text = "Sửa";
            this.bt_sua.UseVisualStyleBackColor = true;
            this.bt_sua.Click += new System.EventHandler(this.bt_sua_Click);
            // 
            // bt_ketthuc
            // 
            this.bt_ketthuc.Image = ((System.Drawing.Image)(resources.GetObject("bt_ketthuc.Image")));
            this.bt_ketthuc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_ketthuc.Location = new System.Drawing.Point(939, 305);
            this.bt_ketthuc.Margin = new System.Windows.Forms.Padding(2);
            this.bt_ketthuc.Name = "bt_ketthuc";
            this.bt_ketthuc.Size = new System.Drawing.Size(91, 43);
            this.bt_ketthuc.TabIndex = 20;
            this.bt_ketthuc.Text = "Thoát";
            this.bt_ketthuc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_ketthuc.UseVisualStyleBackColor = true;
            this.bt_ketthuc.Click += new System.EventHandler(this.bt_ketthuc_Click);
            // 
            // bt_chomuon
            // 
            this.bt_chomuon.Image = ((System.Drawing.Image)(resources.GetObject("bt_chomuon.Image")));
            this.bt_chomuon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_chomuon.Location = new System.Drawing.Point(513, 304);
            this.bt_chomuon.Margin = new System.Windows.Forms.Padding(2);
            this.bt_chomuon.Name = "bt_chomuon";
            this.bt_chomuon.Size = new System.Drawing.Size(101, 43);
            this.bt_chomuon.TabIndex = 19;
            this.bt_chomuon.Text = "Cho Mượn";
            this.bt_chomuon.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_chomuon.UseVisualStyleBackColor = true;
            this.bt_chomuon.Click += new System.EventHandler(this.bt_chomuon_Click);
            // 
            // bt_muonmoi
            // 
            this.bt_muonmoi.Image = ((System.Drawing.Image)(resources.GetObject("bt_muonmoi.Image")));
            this.bt_muonmoi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_muonmoi.Location = new System.Drawing.Point(379, 304);
            this.bt_muonmoi.Margin = new System.Windows.Forms.Padding(2);
            this.bt_muonmoi.Name = "bt_muonmoi";
            this.bt_muonmoi.Size = new System.Drawing.Size(94, 43);
            this.bt_muonmoi.TabIndex = 18;
            this.bt_muonmoi.Text = "Mượn Mới";
            this.bt_muonmoi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_muonmoi.UseVisualStyleBackColor = true;
            this.bt_muonmoi.Click += new System.EventHandler(this.bt_muonmoi_Click);
            // 
            // CapNhatMuonSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(1091, 443);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.bt_Xoa);
            this.Controls.Add(this.bt_sua);
            this.Controls.Add(this.bt_ketthuc);
            this.Controls.Add(this.bt_chomuon);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.bt_muonmoi);
            this.Controls.Add(this.GroupBox2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "CapNhatMuonSach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập Nhật Mượn Sách";
            this.Load += new System.EventHandler(this.CapNhatMuonSach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.ComboBox cb_masach;
        internal System.Windows.Forms.DateTimePicker date_nghen;
        internal System.Windows.Forms.DateTimePicker date_ngmuon;
        internal System.Windows.Forms.TextBox tb_sl_muon;
        internal System.Windows.Forms.Button bt_Xoa;
        internal System.Windows.Forms.Button bt_sua;
        internal System.Windows.Forms.Button bt_ketthuc;
        internal System.Windows.Forms.Button bt_chomuon;
        internal System.Windows.Forms.ComboBox cb_madg;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.Label Label9;
        internal System.Windows.Forms.Button bt_muonmoi;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Label Label6;
    }
}