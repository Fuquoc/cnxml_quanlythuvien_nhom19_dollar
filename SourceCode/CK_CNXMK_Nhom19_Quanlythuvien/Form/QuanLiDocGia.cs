﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien.BLL;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class QuanLiDocGia : Form
    {
        QuanLiDocGiaBLL qldg_bll;

        public QuanLiDocGia()
        {
            InitializeComponent();
            qldg_bll = new QuanLiDocGiaBLL();
        }

        public void Show_DocGia()
        {
            DataTable dt = qldg_bll.getDocGia();
            DataGridView1.DataSource = dt;

        }

        private void QuanLiDocGia_Load(object sender, EventArgs e)
        {
            Show_DocGia();
        }

        public bool CheckData()
        {
            if (string.IsNullOrEmpty(tb_madg.Text))

            {
                MessageBox.Show("Vui lòng nhập dữ liệu! ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                tb_madg.Focus();
                return false;
            }
            return true;
        }

        private void bt_them_Click(object sender, EventArgs e)
        {
            tb_madg.Enabled = true;
            tb_madg.ResetText();
            tb_tendg.ResetText();
            tb_lop.ResetText();
            radio_Nam.Checked = false;
            radio_Nu.Checked = false;
            bt_them.Enabled = false;
            bt_Save.Enabled = true;
            bt_sua.Enabled = false;
            bt_xoa.Enabled = false;
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                tb_madg.Enabled = true;
                tb_madg.Text = DataGridView1.Rows[index].Cells["MaDG"].Value.ToString();
                tb_tendg.Text = DataGridView1.Rows[index].Cells["TenDG"].Value.ToString();
                tb_lop.Text = DataGridView1.Rows[index].Cells["Lop"].Value.ToString();
                if(DataGridView1.Rows[index].Cells["GioiTinh"].Value.ToString()=="Nam")
                {
                    radio_Nam.Checked = true;
                }
                else
                {
                    radio_Nu.Checked = true;
                }                                  
                
            }
        }
        private void bt_Save_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn lưu không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {
                    DocGia dg = new DocGia();
                    dg.MaDG = tb_madg.Text;
                    dg.TenDG = tb_tendg.Text;
                    if (radio_Nam.Checked == true)
                    {
                        dg.GioiTinh = "Nam";
                    }
                    else
                    {
                        dg.GioiTinh = "Nữ";
                    }
                    dg.Lop = tb_lop.Text;
                    if (qldg_bll.InsertDocGia(dg))
                    {
                        MessageBox.Show("Thêm thành công ", "Thông báo ", MessageBoxButtons.OK,MessageBoxIcon.Information);
                        Show_DocGia();
                    }

                    else
                        MessageBox.Show("lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
               

            }
            bt_them.Enabled = true;
            bt_Save.Enabled = false;
            bt_sua.Enabled = true;
            bt_xoa.Enabled = true;
        }

        private void bt_sua_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn sửa không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {

                    DocGia dg = new DocGia();
                    tb_madg.Enabled = false;
                    dg.MaDG = tb_madg.Text;
                    dg.TenDG = tb_tendg.Text;
                    if (radio_Nam.Checked == true)
                    {
                        dg.GioiTinh = "Nam";
                    }
                    else
                    {
                        dg.GioiTinh = "Nữ";
                    }
                    dg.Lop = tb_lop.Text;

                    if (qldg_bll.UpdateDocGIa(dg))
                    {
                        
                        
                        MessageBox.Show("Sửa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Show_DocGia();
                    }
                    else
                        MessageBox.Show("lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }

        }

        private void bt_xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa hay không??", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                DocGia dg = new DocGia();
                dg.MaDG = tb_madg.Text;

                if (qldg_bll.DeleteDocGia(dg))
                {
                    MessageBox.Show("Xóa thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Show_DocGia();
                }
                else
                    MessageBox.Show("lỗi, xin thử lại!", "thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            tb_madg.Text = "";
            tb_tendg.Text = "";
            radio_Nam.Checked = false;
            radio_Nu.Checked = false;
            tb_lop.Text = "";
            
        }
    }
}
