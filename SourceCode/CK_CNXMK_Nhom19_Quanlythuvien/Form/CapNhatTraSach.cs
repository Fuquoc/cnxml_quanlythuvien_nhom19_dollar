﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien.BLL;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class CapNhatTraSach : Form
    {
        CapNhatTraSachBLL cnts;
        public CapNhatTraSach()
        {
            InitializeComponent();
            cnts = new CapNhatTraSachBLL();
        }

        public void ShowTraSach()
        {
            DataTable dt1 = cnts.getTraSach();
            DataGridView1.DataSource = dt1;


        }

        private void CapNhatTraSach_Load(object sender, EventArgs e)
        {
            ShowTraSach();
            ShowDG();
            ShowMaSach();
        }

       

        private void DataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                cb_madg.Text = DataGridView1.Rows[index].Cells["MaDG"].Value.ToString();
                cb_maSach.Text = DataGridView1.Rows[index].Cells["MaSach"].Value.ToString();
                tb_sl.Text = DataGridView1.Rows[index].Cells["SoLuong"].Value.ToString();
                ng_muon.Text = DataGridView1.Rows[index].Cells["NgayMuon"].Value.ToString();
                ng_hen.Text = DataGridView1.Rows[index].Cells["NgayHenTra"].Value.ToString();
                ng_tra.Text = DataGridView1.Rows[index].Cells["NgayTra"].Value.ToString();

            }
        }

        public void ShowDG()
        {
            DataTable dt = cnts.XuatDG();
            cb_madg.DataSource = dt;
            cb_madg.ValueMember = "MaDG";
        }

        public void ShowMaSach()
        {
            DataTable dt = cnts.XuatMaSach();
            cb_maSach.DataSource = dt;
            cb_maSach.ValueMember = "MaSach";
        }

        private void bt_trasach_Click(object sender, EventArgs e)
        {
            cb_madg.Text = "";
            cb_maSach.Text = "";
            tb_sl.Text = "";
            bt_trasach.Enabled = false;
            bt_save.Enabled = true;
            bt_sua.Enabled = false;
            bt_Xoa.Enabled = false;
            ng_muon.ResetText();
            ng_hen.ResetText();
            ng_tra.ResetText();
        }

        private void bt_save_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn lưu không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {
                    MuonSach ms = new MuonSach();
                    ms.MaDG = cb_madg.Text;
                    ms.MaSach = cb_maSach.Text;
                    ms.SoLuong = tb_sl.Text;
                    ms.NgayMuon = ng_muon.Value.ToString("yyyy/MM/dd");
                    ms.NgayHenTra = ng_hen.Value.ToString("yyyy/MM/dd");
                    ms.NgayTra = ng_tra.Value.ToString("yyyy/MM/dd");


                    if (cnts.InsertTraSach(ms))
                    {
                        MessageBox.Show("Thêm thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ShowTraSach();
                        cb_madg.Text = "";
                        cb_maSach.Text = "";
                        tb_sl.Text = "";
                        bt_trasach.Enabled = true;
                        bt_save.Enabled = false;
                        bt_sua.Enabled = true;
                        bt_Xoa.Enabled = true;
                        ng_muon.ResetText();
                        ng_hen.ResetText();
                        ng_tra.ResetText();
                    }

                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }
        }
        public bool CheckData()
        {
            if (string.IsNullOrEmpty(cb_maSach.Text) && string.IsNullOrEmpty(cb_madg.Text))
            {
                MessageBox.Show("Vui lòng nhập dữ liệu ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cb_maSach.Focus();
                cb_madg.Focus();
                return false;
            }
            return true;
        }
        private void bt_thoat_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }
        }

        private void bt_sua_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn sửa không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {

                    MuonSach ms = new MuonSach();

                    ms.MaDG = cb_madg.Text;
                    ms.MaSach = cb_maSach.Text;
                    ms.SoLuong = tb_sl.Text;
                    ms.NgayMuon = ng_muon.Value.ToString("yyyy/MM/dd");
                    ms.NgayHenTra = ng_hen.Value.ToString("yyyy/MM/dd");
                    ms.NgayTra = ng_hen.Value.ToString("yyyy/MM/dd");
                    if (cnts.UpdateTraSach(ms))
                    {
                        ShowTraSach();
                        MessageBox.Show("Sửa thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                       
                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }
        }

        private void bt_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa hay không??", "thông báo lỗi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                MuonSach ms = new MuonSach();
                ms.MaSach = cb_maSach.Text;
                ms.MaDG = cb_madg.Text;
                if (cnts.DeleteTraSachh(ms))
                {
                    ShowTraSach();
                    MessageBox.Show("Xóa thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                    
                else
                    MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}
