﻿
namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class CapNhatTraSach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapNhatTraSach));
            this.cb_maSach = new System.Windows.Forms.ComboBox();
            this.ng_tra = new System.Windows.Forms.DateTimePicker();
            this.ng_hen = new System.Windows.Forms.DateTimePicker();
            this.ng_muon = new System.Windows.Forms.DateTimePicker();
            this.Label6 = new System.Windows.Forms.Label();
            this.Label5 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.tb_sl = new System.Windows.Forms.TextBox();
            this.cb_madg = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.bt_sua = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.bt_Xoa = new System.Windows.Forms.Button();
            this.Label7 = new System.Windows.Forms.Label();
            this.bt_thoat = new System.Windows.Forms.Button();
            this.bt_save = new System.Windows.Forms.Button();
            this.bt_trasach = new System.Windows.Forms.Button();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cb_maSach
            // 
            this.cb_maSach.FormattingEnabled = true;
            this.cb_maSach.Location = new System.Drawing.Point(180, 98);
            this.cb_maSach.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_maSach.Name = "cb_maSach";
            this.cb_maSach.Size = new System.Drawing.Size(168, 24);
            this.cb_maSach.TabIndex = 12;
            // 
            // ng_tra
            // 
            this.ng_tra.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ng_tra.Location = new System.Drawing.Point(616, 161);
            this.ng_tra.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ng_tra.Name = "ng_tra";
            this.ng_tra.Size = new System.Drawing.Size(153, 22);
            this.ng_tra.TabIndex = 11;
            // 
            // ng_hen
            // 
            this.ng_hen.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ng_hen.Location = new System.Drawing.Point(616, 89);
            this.ng_hen.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ng_hen.Name = "ng_hen";
            this.ng_hen.Size = new System.Drawing.Size(153, 22);
            this.ng_hen.TabIndex = 10;
            // 
            // ng_muon
            // 
            this.ng_muon.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.ng_muon.Location = new System.Drawing.Point(616, 33);
            this.ng_muon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ng_muon.Name = "ng_muon";
            this.ng_muon.Size = new System.Drawing.Size(153, 22);
            this.ng_muon.TabIndex = 9;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(479, 164);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(67, 16);
            this.Label6.TabIndex = 8;
            this.Label6.Text = "Ngày Trả:";
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(479, 98);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(95, 16);
            this.Label5.TabIndex = 7;
            this.Label5.Text = "Ngày Hẹn Trả:";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(479, 39);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(79, 16);
            this.Label4.TabIndex = 6;
            this.Label4.Text = "Ngày Mượn:";
            // 
            // DataGridView1
            // 
            this.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(41, 386);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowHeadersWidth = 51;
            this.DataGridView1.RowTemplate.Height = 24;
            this.DataGridView1.Size = new System.Drawing.Size(861, 155);
            this.DataGridView1.TabIndex = 19;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick_1);
            // 
            // tb_sl
            // 
            this.tb_sl.Location = new System.Drawing.Point(180, 161);
            this.tb_sl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_sl.Name = "tb_sl";
            this.tb_sl.Size = new System.Drawing.Size(169, 22);
            this.tb_sl.TabIndex = 5;
            // 
            // cb_madg
            // 
            this.cb_madg.FormattingEnabled = true;
            this.cb_madg.Location = new System.Drawing.Point(180, 27);
            this.cb_madg.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_madg.Name = "cb_madg";
            this.cb_madg.Size = new System.Drawing.Size(169, 24);
            this.cb_madg.TabIndex = 3;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(17, 161);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(103, 16);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "Số Lượng Mượn:";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(17, 98);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(63, 16);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "Mã Sách:";
            // 
            // bt_sua
            // 
            this.bt_sua.Image = ((System.Drawing.Image)(resources.GetObject("bt_sua.Image")));
            this.bt_sua.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_sua.Location = new System.Drawing.Point(419, 267);
            this.bt_sua.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_sua.Name = "bt_sua";
            this.bt_sua.Size = new System.Drawing.Size(117, 50);
            this.bt_sua.TabIndex = 20;
            this.bt_sua.Text = "     Sửa";
            this.bt_sua.UseVisualStyleBackColor = true;
            this.bt_sua.Click += new System.EventHandler(this.bt_sua_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(17, 37);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(80, 16);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Mã Độc Giả:";
            // 
            // bt_Xoa
            // 
            this.bt_Xoa.Image = ((System.Drawing.Image)(resources.GetObject("bt_Xoa.Image")));
            this.bt_Xoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_Xoa.Location = new System.Drawing.Point(609, 267);
            this.bt_Xoa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_Xoa.Name = "bt_Xoa";
            this.bt_Xoa.Size = new System.Drawing.Size(117, 50);
            this.bt_Xoa.TabIndex = 21;
            this.bt_Xoa.Text = "        Xóa";
            this.bt_Xoa.UseVisualStyleBackColor = true;
            this.bt_Xoa.Click += new System.EventHandler(this.bt_Xoa_Click);
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Times New Roman", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.Location = new System.Drawing.Point(36, 340);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(218, 25);
            this.Label7.TabIndex = 17;
            this.Label7.Text = "Danh Sách Trả Sách";
            // 
            // bt_thoat
            // 
            this.bt_thoat.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.thoat;
            this.bt_thoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_thoat.Location = new System.Drawing.Point(783, 267);
            this.bt_thoat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_thoat.Name = "bt_thoat";
            this.bt_thoat.Size = new System.Drawing.Size(93, 50);
            this.bt_thoat.TabIndex = 16;
            this.bt_thoat.Text = "Thoát";
            this.bt_thoat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_thoat.UseVisualStyleBackColor = true;
            this.bt_thoat.Click += new System.EventHandler(this.bt_thoat_Click);
            // 
            // bt_save
            // 
            this.bt_save.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.save;
            this.bt_save.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_save.Location = new System.Drawing.Point(221, 267);
            this.bt_save.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_save.Name = "bt_save";
            this.bt_save.Size = new System.Drawing.Size(117, 50);
            this.bt_save.TabIndex = 15;
            this.bt_save.Text = "    Lưu";
            this.bt_save.UseVisualStyleBackColor = true;
            this.bt_save.Click += new System.EventHandler(this.bt_save_Click);
            // 
            // bt_trasach
            // 
            this.bt_trasach.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.add;
            this.bt_trasach.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_trasach.Location = new System.Drawing.Point(41, 267);
            this.bt_trasach.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_trasach.Name = "bt_trasach";
            this.bt_trasach.Size = new System.Drawing.Size(117, 50);
            this.bt_trasach.TabIndex = 14;
            this.bt_trasach.Text = "Trả Sách";
            this.bt_trasach.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_trasach.UseVisualStyleBackColor = true;
            this.bt_trasach.Click += new System.EventHandler(this.bt_trasach_Click);
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.cb_maSach);
            this.GroupBox1.Controls.Add(this.ng_tra);
            this.GroupBox1.Controls.Add(this.ng_hen);
            this.GroupBox1.Controls.Add(this.ng_muon);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Controls.Add(this.Label5);
            this.GroupBox1.Controls.Add(this.Label4);
            this.GroupBox1.Controls.Add(this.tb_sl);
            this.GroupBox1.Controls.Add(this.cb_madg);
            this.GroupBox1.Controls.Add(this.Label3);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Location = new System.Drawing.Point(41, 47);
            this.GroupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.GroupBox1.Size = new System.Drawing.Size(835, 202);
            this.GroupBox1.TabIndex = 13;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Thực Hiện Trả Sách";
            // 
            // CapNhatTraSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AntiqueWhite;
            this.ClientSize = new System.Drawing.Size(941, 585);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.bt_sua);
            this.Controls.Add(this.bt_Xoa);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.bt_thoat);
            this.Controls.Add(this.bt_save);
            this.Controls.Add(this.bt_trasach);
            this.Controls.Add(this.GroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CapNhatTraSach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CapNhatTraSach";
            this.Load += new System.EventHandler(this.CapNhatTraSach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.ComboBox cb_maSach;
        internal System.Windows.Forms.DateTimePicker ng_tra;
        internal System.Windows.Forms.DateTimePicker ng_hen;
        internal System.Windows.Forms.DateTimePicker ng_muon;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.TextBox tb_sl;
        internal System.Windows.Forms.ComboBox cb_madg;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Button bt_sua;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button bt_Xoa;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.Button bt_thoat;
        internal System.Windows.Forms.Button bt_save;
        internal System.Windows.Forms.Button bt_trasach;
        internal System.Windows.Forms.GroupBox GroupBox1;
    }
}