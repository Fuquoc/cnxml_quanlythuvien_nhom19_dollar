﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CK_CNXMK_Nhom19_Quanlythuvien.BLL;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class CapNhatMuonSach : Form
    {
        CapNhatMuonSachBLL cnms;
        public CapNhatMuonSach()
        {
            InitializeComponent();
            cnms = new CapNhatMuonSachBLL();
        }

        public void ShowMuonSach()
        {
            DataTable dt1 = cnms.getMuonSach();
            DataGridView1.DataSource = dt1;


        }

        private void CapNhatMuonSach_Load(object sender, EventArgs e)
        {
            ShowMuonSach();
            ShowDG();
            ShowMaSach();
            
        }
        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int index = e.RowIndex;
            if (index >= 0)
            {
                cb_madg.Text = DataGridView1.Rows[index].Cells["MaDG"].Value.ToString();
                cb_masach.Text = DataGridView1.Rows[index].Cells["MaSach"].Value.ToString();
                tb_sl_muon.Text = DataGridView1.Rows[index].Cells["SoLuong"].Value.ToString();
                date_ngmuon.Text = DataGridView1.Rows[index].Cells["NgayMuon"].Value.ToString();
                date_nghen.Text = DataGridView1.Rows[index].Cells["NgayHenTra"].Value.ToString();

            }
        }
        public void ShowDG()
        {
            DataTable dt = cnms.XuatDG();
            cb_madg.DataSource = dt;
            cb_madg.ValueMember = "MaDG";
        }
        public void ShowMaSach()
        {
            DataTable dt = cnms.XuatMaSach();
            cb_masach.DataSource = dt;
            cb_masach.ValueMember = "MaSach";
        }
        public bool CheckData()
        {
            if (string.IsNullOrEmpty(cb_masach.Text ) && string.IsNullOrEmpty(cb_madg.Text))
            {
                MessageBox.Show("Vui lòng nhập dữ liệu ", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cb_masach.Focus();
                cb_madg.Focus();
                return false;
            }
            return true;
        }
       
        private void bt_muonmoi_Click(object sender, EventArgs e)
        {

            cb_madg.Text = "";
            cb_masach.Text = "";
            tb_sl_muon.Text = "";
            bt_muonmoi.Enabled = false;
            bt_chomuon.Enabled = true;
            bt_sua.Enabled = false;
            bt_Xoa.Enabled = false;
            date_ngmuon.ResetText();
            date_nghen.ResetText();
        }

        private void bt_ketthuc_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }

        }
        private void bt_chomuon_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn lưu không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {
                    MuonSach ms = new MuonSach();
                    ms.MaDG = cb_madg.Text;
                    ms.MaSach = cb_masach.Text;
                    ms.SoLuong = tb_sl_muon.Text;
                    ms.NgayMuon = date_ngmuon.Value.ToString("yyyy/MM/dd");
                    ms.NgayHenTra = date_nghen.Value.ToString("yyyy/MM/dd");


                    if (cnms.InsertMuonSach(ms))
                    {
                        MessageBox.Show("Thêm thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ShowMuonSach();
                        cb_madg.Text = "";
                        cb_masach.Text = "";
                        tb_sl_muon.Text = "";
                        bt_muonmoi.Enabled = true;
                        bt_chomuon.Enabled = false;
                        bt_sua.Enabled = true;
                        bt_Xoa.Enabled = true;
                        date_ngmuon.ResetText();
                        date_nghen.ResetText();
                    }

                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }
        }

        private void bt_Xoa_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Bạn có muốn xóa hay không??", "thông báo lỗi", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                MuonSach ms = new MuonSach();
                ms.MaSach = cb_masach.Text;
                ms.MaDG = cb_madg.Text;
                if (cnms.DeleteMuonSach(ms))
                {
                    ShowMuonSach();
                    MessageBox.Show("Xóa thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                    
                else
                    MessageBox.Show("Lỗi, xin thử lại!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void bt_sua_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn có muốn sửa không?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                if (CheckData())
                {

                    MuonSach ms = new MuonSach();
                    
                    ms.MaDG = cb_madg.Text;
                    ms.MaSach = cb_masach.Text;
                    ms.SoLuong = tb_sl_muon.Text;
                    ms.NgayMuon = date_ngmuon.Value.ToString("yyyy/MM/dd");
                    ms.NgayHenTra = date_nghen.Value.ToString("yyyy/MM/dd");
                    if (cnms.UpdateMuonSach(ms))
                    {
                        ShowMuonSach();
                        MessageBox.Show("Sửa thành công ", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                       
                    else
                        MessageBox.Show("Lỗi, xin thử lại!", "Thông báo ", MessageBoxButtons.OK, MessageBoxIcon.Stop);

                }
            }
        }

   
    }
}
