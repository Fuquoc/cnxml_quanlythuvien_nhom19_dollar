﻿using CK_CNXMK_Nhom19_Quanlythuvien.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class taoTaiKhoan : Form
    {
        FileXML fileXML = new FileXML();
        DangNhapHT dn = new DangNhapHT();
        string Id, Pass, Name, SDT, DiaChi;
        public taoTaiKhoan()
        {
            InitializeComponent();
        }

        private void bt_thoat_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }
        }

        private void taoTaiKhoan_Load(object sender, EventArgs e)
        {
            saveButton.Visible = false;
            txtAccount.Enabled = false;
            hienthiTTTK();
        }
        public void hienthiTTTK()
        {

            DataTable dt = new DataTable();
            dt = fileXML.HienThi(HeThong.BangTaiKhoan);
            dgvTK.DataSource = dt;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
             if (txtAccount.Text.Equals("") || txtPass.Text.Equals("") || (txtName.Text.Equals(""))  || (txtPhone.Text.Equals("")) || (txt_address.Text.Equals("")))
            {
                MessageBox.Show("Không được bỏ trống các trường!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtAccount.Focus();
            }
            else
            {
                    LoadDuLieu();
                    dn.suaTaiKhoan(Id, Pass, Name, SDT, DiaChi);
                     MessageBox.Show("Ok");
                    hienthiTTTK();

                
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (txtAccount.Text.Equals("") || txtPass.Text.Equals("") || (txtName.Text.Equals("")) || (txtPhone.Text.Equals("")) || (txt_address.Text.Equals("")))
            {
                MessageBox.Show("Không được bỏ trống các trường!", "Lỗi!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                txtAccount.Focus();
            }
            else
            {

                if (dn.kiemtraTTTK(txtAccount.Text) == true)
                {
                    MessageBox.Show("Tên đăng nhập đã tồn tại");
                    txtAccount.Text = "";
                    txtPass.Text = "";
                    txtName.Text = "";
                    txtPhone.Text = "";
                    txt_address.Text = "";
                    txtAccount.Focus();

                }
                else
                {
                    dn.dangkiTaiKhoan(txtAccount.Text, txtPass.Text, txtName.Text, txtPhone.Text, txt_address.Text);
                    MessageBox.Show("Ok");
                    saveButton.Visible = false;
                    btnAdd.Visible = true;
                    txtAccount.Enabled = false;
                    hienthiTTTK();
                }

            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int d = dgvTK.CurrentRow.Index;
            txtAccount.Text = dgvTK.Rows[d].Cells[0].Value.ToString();
            txtPass.Text = dgvTK.Rows[d].Cells[1].Value.ToString();
            txtName.Text = dgvTK.Rows[d].Cells[3].Value.ToString();
            txtPhone.Text = dgvTK.Rows[d].Cells[4].Value.ToString();
            txt_address.Text = dgvTK.Rows[d].Cells[5].Value.ToString();

        }
        public void LoadDuLieu()
        {
            Id = txtAccount.Text;
            Pass = txtPass.Text;
            Name = txtName.Text;
            SDT = txtPhone.Text;
            DiaChi = txt_address.Text;


        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            saveButton.Visible = true;
            btnAdd.Visible = false;
            txtAccount.Enabled = true;
            txtAccount.Text = "";
            txtPass.Text = "";
            txtName.Text = "";
            txtPhone.Text = "";
            txt_address.Text = "";
            txtAccount.Focus();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            dn.xoaTK(txtAccount.Text);
            MessageBox.Show("Ok");
            hienthiTTTK();
        }
    }
}
