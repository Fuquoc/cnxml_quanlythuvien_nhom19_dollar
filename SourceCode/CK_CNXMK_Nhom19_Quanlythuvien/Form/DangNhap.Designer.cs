﻿
namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class DangNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_pass = new System.Windows.Forms.TextBox();
            this.tb_tenDN = new System.Windows.Forms.TextBox();
            this.Button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tb_pass
            // 
            this.tb_pass.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_pass.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.tb_pass.Location = new System.Drawing.Point(47, 161);
            this.tb_pass.MinimumSize = new System.Drawing.Size(2, 55);
            this.tb_pass.Multiline = true;
            this.tb_pass.Name = "tb_pass";
            this.tb_pass.PasswordChar = '.';
            this.tb_pass.Size = new System.Drawing.Size(330, 55);
            this.tb_pass.TabIndex = 12;
            this.tb_pass.Text = "xxxxxxxxxxxxxxxxxxxxx";
            this.tb_pass.WordWrap = false;
            // 
            // tb_tenDN
            // 
            this.tb_tenDN.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tb_tenDN.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.tb_tenDN.Location = new System.Drawing.Point(47, 90);
            this.tb_tenDN.MaximumSize = new System.Drawing.Size(330, 55);
            this.tb_tenDN.Multiline = true;
            this.tb_tenDN.Name = "tb_tenDN";
            this.tb_tenDN.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tb_tenDN.Size = new System.Drawing.Size(330, 55);
            this.tb_tenDN.TabIndex = 11;
            this.tb_tenDN.Text = "Tài khoản";
            this.tb_tenDN.WordWrap = false;
            this.tb_tenDN.TextChanged += new System.EventHandler(this.tb_tenDN_TextChanged);
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.Button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button1.ForeColor = System.Drawing.Color.White;
            this.Button1.Location = new System.Drawing.Point(39, 286);
            this.Button1.Name = "Button1";
            this.Button1.Size = new System.Drawing.Size(345, 60);
            this.Button1.TabIndex = 13;
            this.Button1.Text = "Đăng Nhập";
            this.Button1.UseVisualStyleBackColor = false;
            this.Button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // DangNhap
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.BackgroundImage = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.login_form_johnnythedesigner;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(419, 358);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.tb_pass);
            this.Controls.Add(this.tb_tenDN);
            this.Font = new System.Drawing.Font("Times New Roman", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "DangNhap";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Đăng Nhập";
            this.Load += new System.EventHandler(this.DangNhap_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        internal System.Windows.Forms.Button Button1;
        internal System.Windows.Forms.TextBox tb_pass;
        internal System.Windows.Forms.TextBox tb_tenDN;
    }
}