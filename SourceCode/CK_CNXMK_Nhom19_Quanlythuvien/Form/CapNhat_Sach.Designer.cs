﻿
namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class CapNhat_Sach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_maTG = new System.Windows.Forms.ComboBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.tb_soluong = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.cb_maloai = new System.Windows.Forms.ComboBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.tb_tensach = new System.Windows.Forms.TextBox();
            this.tb_masach = new System.Windows.Forms.TextBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.lb_masach = new System.Windows.Forms.Label();
            this.LB_CTS = new System.Windows.Forms.Label();
            this.bt_xoa = new System.Windows.Forms.Button();
            this.bt_sua = new System.Windows.Forms.Button();
            this.bt_luu = new System.Windows.Forms.Button();
            this.bt_them = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cb_maTG
            // 
            this.cb_maTG.FormattingEnabled = true;
            this.cb_maTG.Location = new System.Drawing.Point(603, 75);
            this.cb_maTG.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_maTG.Name = "cb_maTG";
            this.cb_maTG.Size = new System.Drawing.Size(180, 24);
            this.cb_maTG.TabIndex = 40;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Location = new System.Drawing.Point(512, 78);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(80, 16);
            this.Label7.TabIndex = 39;
            this.Label7.Text = "Mã Tác Giả:";
            // 
            // DataGridView1
            // 
            this.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(34, 357);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowHeadersWidth = 51;
            this.DataGridView1.Size = new System.Drawing.Size(933, 181);
            this.DataGridView1.TabIndex = 38;
            this.DataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // tb_soluong
            // 
            this.tb_soluong.Location = new System.Drawing.Point(602, 138);
            this.tb_soluong.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_soluong.Name = "tb_soluong";
            this.tb_soluong.Size = new System.Drawing.Size(181, 22);
            this.tb_soluong.TabIndex = 31;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Location = new System.Drawing.Point(522, 138);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(67, 16);
            this.Label5.TabIndex = 30;
            this.Label5.Text = "Số Lượng:";
            // 
            // cb_maloai
            // 
            this.cb_maloai.FormattingEnabled = true;
            this.cb_maloai.Location = new System.Drawing.Point(296, 135);
            this.cb_maloai.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_maloai.Name = "cb_maloai";
            this.cb_maloai.Size = new System.Drawing.Size(176, 24);
            this.cb_maloai.TabIndex = 29;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(192, 138);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(92, 16);
            this.Label3.TabIndex = 28;
            this.Label3.Text = "Mã Loại Sách:";
            // 
            // tb_tensach
            // 
            this.tb_tensach.Location = new System.Drawing.Point(296, 107);
            this.tb_tensach.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_tensach.Name = "tb_tensach";
            this.tb_tensach.Size = new System.Drawing.Size(176, 22);
            this.tb_tensach.TabIndex = 27;
            // 
            // tb_masach
            // 
            this.tb_masach.Location = new System.Drawing.Point(296, 75);
            this.tb_masach.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_masach.Name = "tb_masach";
            this.tb_masach.Size = new System.Drawing.Size(176, 22);
            this.tb_masach.TabIndex = 26;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(192, 107);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(68, 16);
            this.Label2.TabIndex = 25;
            this.Label2.Text = "Tên Sách:";
            // 
            // lb_masach
            // 
            this.lb_masach.AutoSize = true;
            this.lb_masach.Location = new System.Drawing.Point(192, 75);
            this.lb_masach.Name = "lb_masach";
            this.lb_masach.Size = new System.Drawing.Size(63, 16);
            this.lb_masach.TabIndex = 24;
            this.lb_masach.Text = "Mã Sách:";
            // 
            // LB_CTS
            // 
            this.LB_CTS.AutoSize = true;
            this.LB_CTS.Font = new System.Drawing.Font("Times New Roman", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LB_CTS.Location = new System.Drawing.Point(332, 9);
            this.LB_CTS.Name = "LB_CTS";
            this.LB_CTS.Size = new System.Drawing.Size(260, 32);
            this.LB_CTS.TabIndex = 23;
            this.LB_CTS.Text = "Quản Lý Danh Sách";
            this.LB_CTS.Click += new System.EventHandler(this.LB_CTS_Click);
            // 
            // bt_xoa
            // 
            this.bt_xoa.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_xoa.ForeColor = System.Drawing.Color.Blue;
            this.bt_xoa.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.xoa;
            this.bt_xoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_xoa.Location = new System.Drawing.Point(599, 198);
            this.bt_xoa.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_xoa.Name = "bt_xoa";
            this.bt_xoa.Size = new System.Drawing.Size(170, 60);
            this.bt_xoa.TabIndex = 35;
            this.bt_xoa.Text = "Xóa";
            this.bt_xoa.UseVisualStyleBackColor = true;
            this.bt_xoa.Click += new System.EventHandler(this.bt_xoa_Click);
            // 
            // bt_sua
            // 
            this.bt_sua.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_sua.ForeColor = System.Drawing.Color.Blue;
            this.bt_sua.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.Change;
            this.bt_sua.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_sua.Location = new System.Drawing.Point(406, 198);
            this.bt_sua.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_sua.Name = "bt_sua";
            this.bt_sua.Size = new System.Drawing.Size(170, 60);
            this.bt_sua.TabIndex = 34;
            this.bt_sua.Text = "Sửa";
            this.bt_sua.UseVisualStyleBackColor = true;
            this.bt_sua.Click += new System.EventHandler(this.bt_sua_Click);
            // 
            // bt_luu
            // 
            this.bt_luu.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_luu.ForeColor = System.Drawing.Color.Blue;
            this.bt_luu.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.save;
            this.bt_luu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_luu.Location = new System.Drawing.Point(406, 286);
            this.bt_luu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_luu.Name = "bt_luu";
            this.bt_luu.Size = new System.Drawing.Size(170, 67);
            this.bt_luu.TabIndex = 33;
            this.bt_luu.Text = "Lưu";
            this.bt_luu.UseVisualStyleBackColor = true;
            this.bt_luu.Click += new System.EventHandler(this.bt_luu_Click);
            // 
            // bt_them
            // 
            this.bt_them.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_them.ForeColor = System.Drawing.Color.Blue;
            this.bt_them.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.add1;
            this.bt_them.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_them.Location = new System.Drawing.Point(207, 198);
            this.bt_them.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bt_them.Name = "bt_them";
            this.bt_them.Size = new System.Drawing.Size(171, 60);
            this.bt_them.TabIndex = 32;
            this.bt_them.Text = "Thêm";
            this.bt_them.UseVisualStyleBackColor = true;
            this.bt_them.Click += new System.EventHandler(this.bt_them_Click);
            // 
            // CapNhat_Sach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.background;
            this.ClientSize = new System.Drawing.Size(979, 541);
            this.Controls.Add(this.cb_maTG);
            this.Controls.Add(this.Label7);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.bt_xoa);
            this.Controls.Add(this.bt_sua);
            this.Controls.Add(this.bt_luu);
            this.Controls.Add(this.bt_them);
            this.Controls.Add(this.tb_soluong);
            this.Controls.Add(this.Label5);
            this.Controls.Add(this.cb_maloai);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.tb_tensach);
            this.Controls.Add(this.tb_masach);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.lb_masach);
            this.Controls.Add(this.LB_CTS);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CapNhat_Sach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cập Nhật Sách";
            this.Load += new System.EventHandler(this.CapNhat_Sach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.ComboBox cb_maTG;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.Button bt_xoa;
        internal System.Windows.Forms.Button bt_sua;
        internal System.Windows.Forms.Button bt_luu;
        internal System.Windows.Forms.Button bt_them;
        internal System.Windows.Forms.TextBox tb_soluong;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.ComboBox cb_maloai;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox tb_tensach;
        internal System.Windows.Forms.TextBox tb_masach;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label lb_masach;
        internal System.Windows.Forms.Label LB_CTS;
    }
}