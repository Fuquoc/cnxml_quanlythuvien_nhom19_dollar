﻿namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    partial class TimKiem_Sach
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimKiem_Sach));
            this.bt_TimKiem = new System.Windows.Forms.Button();
            this.bt_thoat = new System.Windows.Forms.Button();
            this.DataGridView1 = new System.Windows.Forms.DataGridView();
            this.bt_xemHet = new System.Windows.Forms.Button();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.rd_maTG = new System.Windows.Forms.RadioButton();
            this.rd_tenSach = new System.Windows.Forms.RadioButton();
            this.rd_maloai = new System.Windows.Forms.RadioButton();
            this.rd_MaSach = new System.Windows.Forms.RadioButton();
            this.gb_timkiem = new System.Windows.Forms.GroupBox();
            this.bt_Lammoi = new System.Windows.Forms.Button();
            this.tb_timkiem = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).BeginInit();
            this.gb_timkiem.SuspendLayout();
            this.SuspendLayout();
            // 
            // bt_TimKiem
            // 
            this.bt_TimKiem.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_TimKiem.ForeColor = System.Drawing.Color.Blue;
            this.bt_TimKiem.Image = ((System.Drawing.Image)(resources.GetObject("bt_TimKiem.Image")));
            this.bt_TimKiem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_TimKiem.Location = new System.Drawing.Point(382, 75);
            this.bt_TimKiem.Margin = new System.Windows.Forms.Padding(2);
            this.bt_TimKiem.Name = "bt_TimKiem";
            this.bt_TimKiem.Size = new System.Drawing.Size(119, 49);
            this.bt_TimKiem.TabIndex = 47;
            this.bt_TimKiem.Text = "Tìm Kiếm";
            this.bt_TimKiem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_TimKiem.UseVisualStyleBackColor = true;
            this.bt_TimKiem.Click += new System.EventHandler(this.bt_TimKiem_Click);
            // 
            // bt_thoat
            // 
            this.bt_thoat.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_thoat.ForeColor = System.Drawing.Color.Blue;
            this.bt_thoat.Image = global::CK_CNXMK_Nhom19_Quanlythuvien.Properties.Resources.thoat;
            this.bt_thoat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_thoat.Location = new System.Drawing.Point(550, 155);
            this.bt_thoat.Margin = new System.Windows.Forms.Padding(2);
            this.bt_thoat.Name = "bt_thoat";
            this.bt_thoat.Size = new System.Drawing.Size(109, 50);
            this.bt_thoat.TabIndex = 46;
            this.bt_thoat.Text = "Thoát";
            this.bt_thoat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_thoat.UseVisualStyleBackColor = true;
            this.bt_thoat.Click += new System.EventHandler(this.bt_thoat_Click);
            // 
            // DataGridView1
            // 
            this.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView1.Location = new System.Drawing.Point(61, 223);
            this.DataGridView1.Margin = new System.Windows.Forms.Padding(2);
            this.DataGridView1.Name = "DataGridView1";
            this.DataGridView1.RowHeadersWidth = 51;
            this.DataGridView1.RowTemplate.Height = 24;
            this.DataGridView1.Size = new System.Drawing.Size(598, 140);
            this.DataGridView1.TabIndex = 45;
            // 
            // bt_xemHet
            // 
            this.bt_xemHet.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_xemHet.ForeColor = System.Drawing.Color.Blue;
            this.bt_xemHet.Image = ((System.Drawing.Image)(resources.GetObject("bt_xemHet.Image")));
            this.bt_xemHet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_xemHet.Location = new System.Drawing.Point(550, 74);
            this.bt_xemHet.Margin = new System.Windows.Forms.Padding(2);
            this.bt_xemHet.Name = "bt_xemHet";
            this.bt_xemHet.Size = new System.Drawing.Size(109, 50);
            this.bt_xemHet.TabIndex = 44;
            this.bt_xemHet.Text = "Xem Hết";
            this.bt_xemHet.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_xemHet.UseVisualStyleBackColor = true;
            this.bt_xemHet.Click += new System.EventHandler(this.bt_xemHet_Click);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(58, 76);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(156, 19);
            this.Label2.TabIndex = 43;
            this.Label2.Text = "Nhập thông tin tìm kiếm:";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.Color.Blue;
            this.Label1.Location = new System.Drawing.Point(245, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(204, 26);
            this.Label1.TabIndex = 42;
            this.Label1.Text = "TÌM KIẾM SÁCH";
            // 
            // rd_maTG
            // 
            this.rd_maTG.AutoSize = true;
            this.rd_maTG.Location = new System.Drawing.Point(105, 59);
            this.rd_maTG.Margin = new System.Windows.Forms.Padding(2);
            this.rd_maTG.Name = "rd_maTG";
            this.rd_maTG.Size = new System.Drawing.Size(75, 17);
            this.rd_maTG.TabIndex = 3;
            this.rd_maTG.Text = "Mã tác giả";
            this.rd_maTG.UseVisualStyleBackColor = true;
            // 
            // rd_tenSach
            // 
            this.rd_tenSach.AutoSize = true;
            this.rd_tenSach.Location = new System.Drawing.Point(4, 59);
            this.rd_tenSach.Margin = new System.Windows.Forms.Padding(2);
            this.rd_tenSach.Name = "rd_tenSach";
            this.rd_tenSach.Size = new System.Drawing.Size(70, 17);
            this.rd_tenSach.TabIndex = 2;
            this.rd_tenSach.Text = "Tên sách";
            this.rd_tenSach.UseVisualStyleBackColor = true;
            // 
            // rd_maloai
            // 
            this.rd_maloai.AutoSize = true;
            this.rd_maloai.Location = new System.Drawing.Point(105, 22);
            this.rd_maloai.Margin = new System.Windows.Forms.Padding(2);
            this.rd_maloai.Name = "rd_maloai";
            this.rd_maloai.Size = new System.Drawing.Size(85, 17);
            this.rd_maloai.TabIndex = 1;
            this.rd_maloai.Text = "Mã loại sách";
            this.rd_maloai.UseVisualStyleBackColor = true;
            // 
            // rd_MaSach
            // 
            this.rd_MaSach.AutoSize = true;
            this.rd_MaSach.Checked = true;
            this.rd_MaSach.Location = new System.Drawing.Point(4, 22);
            this.rd_MaSach.Margin = new System.Windows.Forms.Padding(2);
            this.rd_MaSach.Name = "rd_MaSach";
            this.rd_MaSach.Size = new System.Drawing.Size(68, 17);
            this.rd_MaSach.TabIndex = 0;
            this.rd_MaSach.TabStop = true;
            this.rd_MaSach.Text = "Mã Sách";
            this.rd_MaSach.UseVisualStyleBackColor = true;
            // 
            // gb_timkiem
            // 
            this.gb_timkiem.Controls.Add(this.rd_maTG);
            this.gb_timkiem.Controls.Add(this.rd_tenSach);
            this.gb_timkiem.Controls.Add(this.rd_maloai);
            this.gb_timkiem.Controls.Add(this.rd_MaSach);
            this.gb_timkiem.Location = new System.Drawing.Point(61, 114);
            this.gb_timkiem.Margin = new System.Windows.Forms.Padding(2);
            this.gb_timkiem.Name = "gb_timkiem";
            this.gb_timkiem.Padding = new System.Windows.Forms.Padding(2);
            this.gb_timkiem.Size = new System.Drawing.Size(309, 92);
            this.gb_timkiem.TabIndex = 48;
            this.gb_timkiem.TabStop = false;
            this.gb_timkiem.Text = "Tìm kiếm theo: ";
            // 
            // bt_Lammoi
            // 
            this.bt_Lammoi.Font = new System.Drawing.Font("Times New Roman", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bt_Lammoi.ForeColor = System.Drawing.Color.Blue;
            this.bt_Lammoi.Image = ((System.Drawing.Image)(resources.GetObject("bt_Lammoi.Image")));
            this.bt_Lammoi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bt_Lammoi.Location = new System.Drawing.Point(382, 155);
            this.bt_Lammoi.Margin = new System.Windows.Forms.Padding(2);
            this.bt_Lammoi.Name = "bt_Lammoi";
            this.bt_Lammoi.Size = new System.Drawing.Size(113, 51);
            this.bt_Lammoi.TabIndex = 50;
            this.bt_Lammoi.Text = "Làm Mới";
            this.bt_Lammoi.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bt_Lammoi.UseVisualStyleBackColor = true;
            this.bt_Lammoi.Click += new System.EventHandler(this.bt_Lammoi_Click);
            // 
            // tb_timkiem
            // 
            this.tb_timkiem.Location = new System.Drawing.Point(230, 75);
            this.tb_timkiem.Margin = new System.Windows.Forms.Padding(2);
            this.tb_timkiem.Name = "tb_timkiem";
            this.tb_timkiem.Size = new System.Drawing.Size(140, 20);
            this.tb_timkiem.TabIndex = 49;
            // 
            // TimKiem_Sach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(679, 390);
            this.Controls.Add(this.bt_TimKiem);
            this.Controls.Add(this.bt_thoat);
            this.Controls.Add(this.DataGridView1);
            this.Controls.Add(this.bt_xemHet);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.gb_timkiem);
            this.Controls.Add(this.bt_Lammoi);
            this.Controls.Add(this.tb_timkiem);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "TimKiem_Sach";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TimKiem_Sach";
            this.Load += new System.EventHandler(this.TimKiem_Sach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView1)).EndInit();
            this.gb_timkiem.ResumeLayout(false);
            this.gb_timkiem.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button bt_TimKiem;
        internal System.Windows.Forms.Button bt_thoat;
        internal System.Windows.Forms.DataGridView DataGridView1;
        internal System.Windows.Forms.Button bt_xemHet;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.RadioButton rd_maTG;
        internal System.Windows.Forms.RadioButton rd_tenSach;
        internal System.Windows.Forms.RadioButton rd_maloai;
        internal System.Windows.Forms.RadioButton rd_MaSach;
        internal System.Windows.Forms.GroupBox gb_timkiem;
        internal System.Windows.Forms.Button bt_Lammoi;
        internal System.Windows.Forms.TextBox tb_timkiem;
    }
}