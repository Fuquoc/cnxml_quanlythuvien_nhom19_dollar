﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    public partial class TimKiem_Sach : Form
    {
        CapNhatSachBLL cnsbll;
        public TimKiem_Sach()
        {
            InitializeComponent();
            cnsbll = new CapNhatSachBLL();
        }
        public void ShowALLSach()
        {
            DataTable dt = cnsbll.getALLSach();
            DataGridView1.DataSource = dt;

        }

       

       

        private void bt_TimKiem_Click(object sender, EventArgs e)
        {
            string value = tb_timkiem.Text;
            if (rd_maloai.Checked==false && rd_MaSach.Checked==false && rd_maTG.Checked==false && rd_tenSach.Checked==false)
            {
                MessageBox.Show("Vui lòng kích chọn thông tin tìm kiếm ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (!string.IsNullOrEmpty(value))
                {
                    if (rd_MaSach.Checked == true)
                    {
                        DataTable dt = cnsbll.Find_MaSach(value);
                        DataGridView1.DataSource = dt;
                    }
                    else
                    {
                        if (rd_tenSach.Checked == true)
                        {
                            DataTable dt = cnsbll.Find_TenSach(value);
                            DataGridView1.DataSource = dt;
                        }
                        else
                        {
                            if (rd_maTG.Checked == true)
                            {
                                DataTable dt = cnsbll.Find_MaTacGia(value);
                                DataGridView1.DataSource = dt;
                            }
                            else
                            {
                                DataTable dt = cnsbll.Find_MaLoaiSach(value);
                                DataGridView1.DataSource = dt;
                            }
                        }
                    }

                }
                else
                {
                    MessageBox.Show("Vui lòng nhập thông tin tìm kiếm ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
                
           
           
        }

        private void TimKiem_Sach_Load(object sender, EventArgs e)
        {
            ShowALLSach();
        }

        private void bt_xemHet_Click(object sender, EventArgs e)
        {
            ShowALLSach();
            rd_maloai.Checked = false;
            rd_MaSach.Checked = false;
            rd_maTG.Checked = false;
            rd_tenSach.Checked = false;
            tb_timkiem.Text = "";
        }

        private void bt_thoat_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Bạn chắc chắc muốn Thoát ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialogResult == DialogResult.Yes)
            {
                HeThong_ThuVien obj = new HeThong_ThuVien();
                obj.Show();
                this.Hide();
            }
            else
            {
                this.Show();
            }
        }

        private void bt_Lammoi_Click(object sender, EventArgs e)
        {
            rd_maloai.Checked = true;
            rd_MaSach.Checked = false;
            rd_maTG.Checked = false;
            rd_tenSach.Checked = false;
            tb_timkiem.Text = "";
        }
    }
}
