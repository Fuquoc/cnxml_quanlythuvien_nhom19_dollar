﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Diagnostics;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    class FileXML
    {
        //string Conn = @"LAPTOP-FJ4KUMOA\SQLEXPRESS;Initial Catalog=QuanLiThuVien;Integrated Security=True";
        string Conn = @"Data Source=LAPTOP-FJ4KUMOA\SQLEXPRESS;Initial Catalog=QuanLiThuVien;Integrated Security=True";
        public DataTable HienThi(string Bang) //loadDataGridView
        {
            DataTable dt = new DataTable();
            string FilePath = Application.StartupPath + "\\" + Bang + ".xml";
            if (File.Exists(FilePath))
            {
                FileStream fsReadXML = new FileStream(FilePath, FileMode.Open);
                dt.ReadXml(fsReadXML);
                fsReadXML.Close();
            }
            else
            {
                TaoXML(Bang);
                FileStream fsReadXML = new FileStream(FilePath, FileMode.Open);
                dt.ReadXml(fsReadXML);
                fsReadXML.Close();
            }

            return dt; //đọc file xml và trả về datatable
        }
        public void TaoXML(string bang)
        {
            SqlConnection con = new SqlConnection(Conn);
            con.Open();
            string sql = "Select* from " + bang; 
            SqlDataAdapter ad = new SqlDataAdapter(sql, con);
            DataTable dt = new DataTable("'" + bang + "'");
            ad.Fill(dt); //fill dữ liệu từ adapter vào datatable
            dt.WriteXml(Application.StartupPath + "\\" + bang + ".xml", XmlWriteMode.WriteSchema);//viết file xml từ datatabe
        }


        public void InsertOrUpDateSQL(string sql)
        {
            SqlConnection con = new SqlConnection(Conn);
            con.Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void Them(String Bang, string noiDung)
        {
            XmlTextReader reader = new XmlTextReader(Bang +".xml"); // đọc file xml từ đường dẫn
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            reader.Close();
            XmlNode currNode; //tạo current node
            XmlDocumentFragment docFrag = doc.CreateDocumentFragment(); //kế thừa xml node
            docFrag.InnerXml = noiDung; //Nhận hoặc đặt đánh dấu đại diện cho nút con của nút này.
            currNode = doc.DocumentElement;  // gán curr node vào node gốc của doc
            currNode.InsertAfter(docFrag, currNode.LastChild);//thêm node docFrag vào sau
            doc.Save(Bang +".xml");

            //Class.HeThong ht = new Class.HeThong();
            //ht.CapNhapSQL("taikhoan");
        }
        public void Xoa(string Bang, string tenFileXML, string xoaTheoTruong, string giaTriTruong)
        {
            Debug.Print(xoaTheoTruong +" " + giaTriTruong);
            string fileName = Application.StartupPath + "\\" + Bang +".xml"; // lấy đường dẫn của filexml
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            XmlNode nodeCu = doc.SelectSingleNode("NewDataSet/" + tenFileXML + "[" + xoaTheoTruong + "='" + giaTriTruong + "']"); //lấy node theo giá trị
            doc.DocumentElement.RemoveChild(nodeCu);//xoa nodecu
            doc.Save(fileName);
            //Class.HeThong ht = new Class.HeThong();
            //ht.CapNhapSQL("taikhoan");
        }

        public void Sua(string Bang, string tenFile, string suaTheoTruong, string giaTriTruong, string noiDung)
        {
            string fileName = Application.StartupPath + "\\" + Bang + ".xml";
            XmlDocument doc = new XmlDocument();
            doc.Load(fileName);
            XmlNode oldHang;
            XmlElement root = doc.DocumentElement;
            oldHang = root.SelectSingleNode("/NewDataSet/" + tenFile + "[" + suaTheoTruong + "='" + giaTriTruong + "']"); //lay Node theo giatritruong
            XmlElement newhang = doc.CreateElement(tenFile);
            newhang.InnerXml = noiDung;
            root.ReplaceChild(newhang, oldHang);
            doc.Save(Bang +".xml");
            
            //Class.HeThong ht = new Class.HeThong();
            //ht.CapNhapSQL("taikhoan");
        }
        public string LayGiaTri(string duongDan, string truongA, string giaTriA, string truongB)
        {
            string giatriB = "";
            DataTable dt = new DataTable();
            dt = HienThi(duongDan);
            int soDongNhanVien = dt.Rows.Count;
            for (int i = 0; i < soDongNhanVien; i++)
            {
                if (dt.Rows[i][truongA].ToString().Trim().Equals(giaTriA))
                {
                    giatriB = dt.Rows[i][truongB].ToString();
                    return giatriB;
                }
            }
            return giatriB;
        }
        public void DoiMatKhau(string nguoiDung, string matKhau)
        {
            XmlDocument doc1 = new XmlDocument();
            doc1.Load(Application.StartupPath + "\\taikhoan.xml");
            XmlNode node1 = doc1.SelectSingleNode("NewDataSet/TaiKhoan[MaNhanVien = '" + nguoiDung + "']");
            if (node1 != null)
            {
                node1.ChildNodes[1].InnerText = matKhau;
                doc1.Save(Application.StartupPath + "\\taikhoan.xml");
            }
        }
       
        
    }
}
