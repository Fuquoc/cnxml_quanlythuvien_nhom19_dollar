﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien.Class
{
    class MuonSach
    {
        public string MaDG { set; get; }
        public string MaSach { set; get; }
        public string SoLuong { set; get; }

        public string NgayMuon { set; get; }
        public string NgayHenTra { set; get; }
        public string NgayTra { set; get; }
        public string TenDG { set; get; }
        public string GioiTinh { set; get; }

        public string Lop { set; get; }
        public string TenSach { set; get; }
    }
}
