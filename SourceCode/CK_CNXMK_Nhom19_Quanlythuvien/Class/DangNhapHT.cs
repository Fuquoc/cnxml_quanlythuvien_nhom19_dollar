﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using CK_CNXMK_Nhom19_Quanlythuvien.DAL;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;
using System.Diagnostics;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Rebar;
using System.IO;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    class DangNhapHT
    {
        FileXML Fxml = new FileXML();

        public DangNhapHT()
        {
            string FilePath = Application.StartupPath + "\\" + HeThong.BangTaiKhoan + ".xml";
            if (!File.Exists(FilePath))
            {
                Fxml.TaoXML(HeThong.BangTaiKhoan);
            }
        }
        //public void layMaQuyen()
        //{
        //    XmlTextReader reader = new XmlTextReader("taikhoan.xml");
        //    XmlDocument doc = new XmlDocument();
        //    doc.Load(reader);
        //    reader.Close();
        //    XmlNode nodeMQ = doc.SelectSingleNode("NewDataSet/TaiKhoan/Quyen");
        //}
        public bool kiemtraDangNhap(string Id, string MatKhau)
        {
            Debug.Print("Đăng Nhập với :" + Id + "  " + MatKhau);
            XmlTextReader reader = new XmlTextReader(HeThong.BangTaiKhoan + ".xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNode node = doc.SelectSingleNode("NewDataSet/_x0027_taikhoan_x0027_[Id='" + Id + "']");
            String pass = node.SelectSingleNode("MatKhau").InnerText;
            reader.Close();
            Debug.Print(pass.Length+  " " + MatKhau.Length);
            if (pass.Split(' ')[0] == MatKhau)
            {
                return true;
                Debug.Write("Trùng Khớp Tài Khoản");
            }
            else
            {
                return false;
            }
        }
        public bool kiemtraTTTK(string Id)
        {
            XmlTextReader reader = new XmlTextReader(HeThong.BangTaiKhoan +".xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNode node = doc.SelectSingleNode("NewDataSet/_x0027_taikhoan_x0027_[Id='" + Id + "']");
            reader.Close();
            bool kq = true;
            if (node != null)
            {
                return kq = true;
            }
            else
            {
                return kq = false;

            }
        }

        //public bool kiemtraTTDN(string Bang, string Id, string MatKhau)
        //{
        //    DataTable dt = new DataTable();
        //    dt = Fxml.HienThi(Bang);
        //    dt.DefaultView.RowFilter = "Id ='" + Id + "' AND MatKhau='" + MatKhau + "'";
        //    if (dt.DefaultView.Count > 0)
        //        return true;
        //    return false;

        //}
        public bool kiemtraMK(string duongdan, string MatKhau)
        {
            DataTable dt = new DataTable();
            dt = Fxml.HienThi(duongdan);
            dt.DefaultView.RowFilter = "MatKhau='" + MatKhau + "'";
            if (dt.DefaultView.Count > 0)
                return true;
            return false;

        }
        public void dangkiTaiKhoan(string Id, string MatKhau,string Ten, string sdt, string diachi)
        {
            string noiDung = "<_x0027_taikhoan_x0027_>" +
                    "<Id>" + Id + "</Id>" +
                    "<MatKhau>" + MatKhau + "</MatKhau>" +
                    "<Quyen>1</Quyen>" +
                    "<TenNV>"+Ten+"</TenNV>"+
                    "<SoDienThoai>"+ sdt + "</SoDienThoai>"+
                    "<DiaChi>"+diachi+"</DiaChi>"+
                    "</_x0027_taikhoan_x0027_>";
            Fxml.Them(HeThong.BangTaiKhoan, noiDung);
        }
        public void xoaTK(string Id)
        {
            Fxml.Xoa(HeThong.BangTaiKhoan, "_x0027_taikhoan_x0027_", "Id", Id);

        }

        public void suaTaiKhoan(string Id, string MatKhau, string Ten, string sdt, string diachi)
        {
            string noiDung =
                    "<Id>" + Id + "</Id>" +
                    "<MatKhau>" + MatKhau + "</MatKhau>" +
                    "<Quyen>1</Quyen>" +
                    "<TenNV>" + Ten + "</TenNV>" +
                    "<SoDienThoai>" + sdt + "</SoDienThoai>" +
                    "<DiaChi>" + diachi + "</DiaChi>";
            Fxml.Sua(HeThong.BangTaiKhoan, "_x0027_taikhoan_x0027_", "Id",Id, noiDung);
        }

        public void DoiMatKhau(string nguoiDung, string matKhau)
        {
           
            
            XmlDocument doc1 = new XmlDocument();
            doc1.Load(Application.StartupPath + "\\taikhoan.xml");
            XmlNode node1 = doc1.SelectSingleNode("NewDataSet/_x0027_taikhoan_x0027_[Id = '" + nguoiDung + "']");
            if (node1 != null)
            {
                node1.ChildNodes[1].InnerText = matKhau;
                doc1.Save(Application.StartupPath + "\\taikhoan.xml");
                HeThong ht = new HeThong();
                ht.CapNhapSQL("taikhoan");
            }
            
        }
     
    }
}
