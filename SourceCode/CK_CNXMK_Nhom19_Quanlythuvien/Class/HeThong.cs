﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien.Class
{
    class HeThong
    {
        public static String BangTaiKhoan = "taikhoan";
        public static String BangSach = "tblSach";
        public static String BangDocGia = "tblDocGia";
        public static String BangTacGia = "tblTacGia";
        public static String BangLoaiSach = "tblLoaiSach";
        public static String BangMuonTraSach = "tblMuonTraSach";
        FileXML Fxml = new FileXML();

        public void TaoXML()
        {
            Fxml.TaoXML(BangTaiKhoan);
            Fxml.TaoXML(BangSach);
            Fxml.TaoXML(BangDocGia);
            Fxml.TaoXML(BangTacGia);
            Fxml.TaoXML(BangLoaiSach);
            Fxml.TaoXML(BangMuonTraSach);
            //Fxml.TaoXML("tblMuonTraSach");
        }

        void CapNhapTungBang(string tenBang)
        {
            //string duongDan = @"" + tenBang + ".xml";
            DataTable table = Fxml.HienThi(tenBang);
            for (int i = 0; i < table.Rows.Count; i++)
            {
                string sql = "insert into " + tenBang + " values(";
                for (int j = 0; j < table.Columns.Count - 1; j++)
                {
                    sql += "N'" + table.Rows[i][j].ToString().Trim() + "',";
                }
                sql += "N'" + table.Rows[i][table.Columns.Count - 1].ToString().Trim() + "'";
                sql += ")";
                //MessageBox.Show(sql);
                Fxml.InsertOrUpDateSQL(sql);
            }
        }
        public void CapNhapSQL(string tenBang)
        {
            //Xóa toàn bộ dữ liệu các bảng
            Fxml.InsertOrUpDateSQL("delete from "+tenBang);     
            //Cập nhập toàn bộ dữ liệu các bảng
            CapNhapTungBang(tenBang);
           
        }
    }
}
