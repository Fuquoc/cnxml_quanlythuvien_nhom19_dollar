﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien
{
    class DocGia
    {
        public string MaDG { set; get; }
        public string TenDG { set; get; }
        public string GioiTinh { set; get; }

        public string Lop { set; get; }
        
    }
}
