﻿using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
   
    class DoiMatKhauDAL
    {
        DataSet ds;
        Dataconnection tc = new Dataconnection();
        SqlDataAdapter da;
        SqlCommand cmd;
        public DoiMatKhauDAL()
        {
            tc = new Dataconnection();
        }

        public bool doiMatKhau(string id,string mk)
        {
            string sql = "UPDATE taikhoan SET MatKhau= @MatKhau WHERE Id=@Id";

            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MatKhau", SqlDbType.Char).Value = mk;
                cmd.Parameters.Add("@Id", SqlDbType.VarChar).Value = id;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception e)
            {
                
                return false;
            }
            return true;
        }
        
    }
}
