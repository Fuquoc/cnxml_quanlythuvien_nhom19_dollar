﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
    class QuanLiTacGiaDAL
    {
        Dataconnection tc;
        SqlDataAdapter da;
        SqlCommand cmd;
        public QuanLiTacGiaDAL()
        {
            tc = new Dataconnection();
        }

        public DataTable getTacGia()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblTacGia";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertTacGia(TacGia tg)
        {
            string sql = "INSERT INTO tblTacGia(MaTG,TenTG,DiaChi) VALUES(@MaTG,@TenTG,@DiaChi)";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaTG", SqlDbType.Char).Value = tg.MaTG;
                cmd.Parameters.Add("@TenTG", SqlDbType.NVarChar).Value = tg.TenTG;
                cmd.Parameters.Add("@DiaChi", SqlDbType.NVarChar).Value = tg.DiaChi;
               

                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool UpdateTacGIa(TacGia tg)
        {
            string sql = "UPDATE tblTacGia SET MaTG= @MaTG, TenTG=@TenTG, DiaChi=@DiaChi WHERE MaTG=@MaTG";

            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaTG", SqlDbType.Char).Value = tg.MaTG;
                cmd.Parameters.Add("@TenTG", SqlDbType.NVarChar).Value = tg.TenTG;
                cmd.Parameters.Add("@DiaChi", SqlDbType.NVarChar).Value = tg.DiaChi;
               

                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool DeleteTacGia(TacGia tg)
        {
            string sql = "DELETE tblTacGia WHERE MaTG=@MaTG";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaTG", SqlDbType.Char).Value = tg.MaTG;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }
        public DataTable FindTacGia(string tg)
        {
            string sql = "SELECT * FROM tblTacGia WHERE MaTG like '%" + tg + "%' TenTG  '%" + tg + "%'";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

    }
}
