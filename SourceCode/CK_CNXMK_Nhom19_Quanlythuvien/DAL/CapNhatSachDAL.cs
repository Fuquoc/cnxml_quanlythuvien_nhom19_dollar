﻿using CK_CNXMK_Nhom19_Quanlythuvien.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
    class CapNhatSachDAL
    {
        DataSet ds;
        Dataconnection tc;
        SqlDataAdapter da;
        SqlCommand cmd;
        FileXML xml = new FileXML();
        public CapNhatSachDAL()
        {
            tc = new Dataconnection();
        }

        public DataTable getALLSach()
        {
            ////tạo lệnh sql
            //string sql = "SELECT * from tblSach";
            ////kết nối sql
            //SqlConnection con = tc.getconnect();
            ////khơi tạo đói tượng lớp sqldata
            //da = new SqlDataAdapter(sql, con);
            //con.Open();
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            //con.Close();
            //return dt;
            return xml.HienThi(HeThong.BangSach);
        }


        public DataTable XuatTG()
        {
            //tạo lệnh sql
            //string sql = "SELECT * from tblTacGia";
            ////kết nối sql
            //SqlConnection con = tc.getconnect();
            ////khơi tạo đói tượng lớp sqldata
            //da = new SqlDataAdapter(sql, con);
            //con.Open();
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            //con.Close();
            //return dt;
            return xml.HienThi(HeThong.BangTacGia);
        }
        public DataTable XuatMaLoaiSach()
        {
            //tạo lệnh sql
            //string sql = "SELECT * from tblLoaiSach";
            ////kết nối sql
            //SqlConnection con = tc.getconnect();
            ////khơi tạo đói tượng lớp sqldata
            //da = new SqlDataAdapter(sql, con);
            //con.Open();
            //DataTable dt = new DataTable();
            //da.Fill(dt);
            //con.Close();
            //return dt;
            return xml.HienThi(HeThong.BangLoaiSach);
        }


        public bool InserttblSach(Sach sach)
        {
            string sql = "INSERT INTO tblSach(MaSach,TenSach,MaLoaiSach,MaTG,SoLuong) VALUES(@MaSach,@TenSach,@MaLoaiSach,@MaTG,@SoLuong)";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = sach.MaSach;
                cmd.Parameters.Add("@TenSach", SqlDbType.NVarChar).Value = sach.TenSach;
                cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Char).Value = sach.MaLoaiSach;
                cmd.Parameters.Add("@MaTG", SqlDbType.NVarChar).Value = sach.MaTG;
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = sach.SoLuong;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool UpdatetblSach(Sach sach)
        {
            string sql = "UPDATE tblSach SET MaSach= @MaSach, TenSach=@TenSach, MaLoaiSach=@MaLoaiSach, MaTG=@MaTG,SoLuong=@SoLuong WHERE MaSach=@MaSach";

            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = sach.MaSach;
                cmd.Parameters.Add("@TenSach", SqlDbType.NVarChar).Value = sach.TenSach;
                cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Char).Value = sach.MaLoaiSach;
                cmd.Parameters.Add("@MaTG", SqlDbType.Char).Value = sach.MaTG;
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = sach.SoLuong;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool DELETEtblSach(Sach sach)
        {
            string sql = "DELETE tblSach WHERE MaSach=@MaSach";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = sach.MaSach;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }

       
        public DataTable Find_TenSach(string sach)
        {
           
            string sql = "SELECT * FROM tblSach WHERE TenSach like N'%" + sach + "%'";

            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable Find_MaSach(string sach)
        {

            string sql = "SELECT * FROM tblSach WHERE MaSach like '%" + sach + "%'";

            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable Find_MaLoaiSach(string sach)
        {

            string sql = "SELECT * FROM tblSach WHERE MaLoaiSach like '%" + sach + "%'";

            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable Find_MaTacGia(string sach)
        {

            string sql = "SELECT * FROM tblSach WHERE MaTG like '%" + sach + "%'";

            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
