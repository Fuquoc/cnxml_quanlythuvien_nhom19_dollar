﻿using CK_CNXMK_Nhom19_Quanlythuvien.Class;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
    class QuanLiDocGiaDAL
    {
        Dataconnection tc;
        SqlDataAdapter da;
        SqlCommand cmd;

        FileXML Fxml = new FileXML();
        public QuanLiDocGiaDAL()
        {
            tc = new Dataconnection();
        }

        public DataTable getDocGia()
        {
            return Fxml.HienThi(HeThong.BangDocGia);
        }

        public bool InsertDocGia(DocGia dg)
        {
            //string sql = "INSERT INTO tblDocGia(MaDG,TenDG,GioiTinh,Lop) VALUES(@MaDG,@TenDG,@GioiTinh,@Lop)";
            //SqlConnection con = tc.getconnect();
            try
            {
                //cmd = new SqlCommand(sql, con);
                //con.Open();
                //cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = dg.MaDG;
                //cmd.Parameters.Add("@TenDG", SqlDbType.NVarChar).Value = dg.TenDG;
                //cmd.Parameters.Add("@GioiTinh", SqlDbType.NVarChar).Value = dg.GioiTinh;
                //cmd.Parameters.Add("@Lop", SqlDbType.NVarChar).Value = dg.Lop;

                //cmd.ExecuteNonQuery();
                //con.Close();

                string noiDung = "<_x0027_tblDocGia_x0027_>" +
                    "<MaDG>" + dg.MaDG + "</MaDG>" +
                    "<TenDG>" + dg.TenDG + "</TenDG>" +
                "<GioiTinh>" + dg.GioiTinh+ "</GioiTinh>" +
                    "<Lop>" + dg.Lop + "</Lop>" +
                    "</_x0027_tblDocGia_x0027_>";
                Fxml.Them(HeThong.BangDocGia, noiDung);
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                return false;
            }
            return true;
        }
        public bool UpdateDocGIa(DocGia dg)
        {
            //string sql = "UPDATE tblDocGia SET MaDG= @MaDG, TenDG=@TenDG, GioiTinh=@GioiTinh, Lop=@Lop  WHERE MaDG=@MaDG";

            //SqlConnection con = tc.getconnect();
            try
            {
                //cmd = new SqlCommand(sql, con);
                //con.Open();
                //cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = dg.MaDG;
                //cmd.Parameters.Add("@TenDG", SqlDbType.NVarChar).Value = dg.TenDG;
                //cmd.Parameters.Add("@GioiTinh", SqlDbType.NVarChar).Value = dg.GioiTinh;
                //cmd.Parameters.Add("@Lop", SqlDbType.NVarChar).Value = dg.Lop;

                //cmd.ExecuteNonQuery();
                //con.Close();
                string noiDung = 
                "<MaDG>" + dg.MaDG + "</MaDG>" +
                "<TenDG>" + dg.TenDG + "</TenDG>" +
                "<GioiTinh>" + dg.GioiTinh + "</GioiTinh>" +
                "<Lop>" + dg.Lop + "</Lop>";
                Fxml.Sua(HeThong.BangDocGia, "_x0027_tblDocGia_x0027_", "MaDG", dg.MaDG, noiDung);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool DeleteDocGia(DocGia dg)
        {
            //string sql = "DELETE tblDocGia WHERE MaDG=@MaDG";
            //SqlConnection con = tc.getconnect();
            try
            {
                //cmd = new SqlCommand(sql, con);
                //con.Open();
                //cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = dg.MaDG;
                //cmd.ExecuteNonQuery();
                //con.Close();

                Fxml.Xoa(HeThong.BangDocGia, "_x0027_tblDocGia_x0027_", "MaDG", dg.MaDG);
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }
        public DataTable FindDocGia(string dg)
        {
            string sql = "SELECT * FROM tblDocGia WHERE MaDG like '%" + dg + "%' or TenDG  '%" + dg + "%'";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
