﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
    class CapNhatTraSachDAL
    {
        Dataconnection tc;
        SqlDataAdapter da;
        SqlCommand cmd;

        public CapNhatTraSachDAL()
        {
            tc = new Dataconnection();
        }

        public DataTable getTraSach()
        {
            //tạo lệnh
            string sql = "select mt.*, dg.TenDG from tblMuonTraSach as mt, tblDocGia as dg where dg.MaDG = mt.MaDG";
            
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable XuatDG()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblDocGia";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable XuatMaSach()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblSach";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertTraSach(MuonSach ms)
        {
            string sql = "INSERT INTO tblMuonTraSach(MaDG,MaSach,SoLuong,NgayMuon,NgayHenTra,NgayTra) VALUES(@MaDG,@MaSach,@SoLuong,@NgayMuon,@NgayHenTra,@NgayTra)";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = ms.MaDG;
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = ms.MaSach;
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = ms.SoLuong;
                cmd.Parameters.Add("@NgayMuon", SqlDbType.Date).Value = ms.NgayMuon;
                cmd.Parameters.Add("@NgayHenTra", SqlDbType.Date).Value = ms.NgayHenTra;
                cmd.Parameters.Add("@NgayTra", SqlDbType.Date).Value = ms.NgayTra;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool UpdateTraSach(MuonSach ms)
        {
            string sql = "UPDATE tblMuonTraSach SET MaDG= @MaDG, MaSach=@MaSach, SoLuong=@SoLuong, NgayMuon=@NgayMuon,NgayHenTra=@NgayHenTra, NgayTra=@NgayTra WHERE MaDG=@MaDG and MaSach=@MaSach";

            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = ms.MaDG;
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = ms.MaSach;
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = ms.SoLuong;
                cmd.Parameters.Add("@NgayMuon", SqlDbType.Date).Value = ms.NgayMuon;
                cmd.Parameters.Add("@NgayHenTra", SqlDbType.Date).Value = ms.NgayHenTra;
                cmd.Parameters.Add("@NgayTra", SqlDbType.Date).Value = ms.NgayTra;

                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool DeleteTraSachh(MuonSach sach)
        {
            string sql = "DELETE tblMuonTraSach WHERE MaDG=@MaDG and MaSach=@MaSach  ";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = sach.MaDG;
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = sach.MaSach;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }

    }
}
