﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CK_CNXMK_Nhom19_Quanlythuvien.Class;

namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
    class CapNhatMuonSachDAL
    {
        Dataconnection tc;
        SqlDataAdapter da;
        SqlCommand cmd;

        public CapNhatMuonSachDAL()
        {
            tc = new Dataconnection();
        }

        public DataTable getMuonSach()
        {
            //tạo lệnh sql
            string sql = "select mt.MaDG , mt.MaSach,mt.SoLuong,mt.NgayMuon,mt.NgayHenTra,dg.TenDG from tblMuonTraSach as mt, tblDocGia as dg, tblSach as s where  s.MaSach = mt.MaSach  and dg.MaDG=mt.MaDG  ";
            //kết nối sql
         
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable XuatDG()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblDocGia";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable XuatMaSach()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblSach";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public DataTable XuatTenSach()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblSach";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InsertMuonSach(MuonSach ms)
        {
            string sql = "INSERT INTO tblMuonTraSach(MaDG,MaSach,SoLuong,NgayMuon,NgayHenTra) VALUES(@MaDG,@MaSach,@SoLuong,@NgayMuon,@NgayHenTra)";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = ms.MaDG;
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = ms.MaSach;
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = ms.SoLuong;
                cmd.Parameters.Add("@NgayMuon", SqlDbType.Date).Value = ms.NgayMuon;
                cmd.Parameters.Add("@NgayHenTra", SqlDbType.Date).Value = ms.NgayHenTra;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool UpdateMuonSach(MuonSach ms)
        {
            string sql = "UPDATE tblMuonTraSach SET MaDG= @MaDG, MaSach=@MaSach, SoLuong=@SoLuong, NgayMuon=@NgayMuon,NgayHenTra=@NgayHenTra WHERE MaDG=@MaDG and MaSach=@MaSach";

            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = ms.MaDG;
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = ms.MaSach;
                cmd.Parameters.Add("@SoLuong", SqlDbType.Int).Value = ms.SoLuong;
                cmd.Parameters.Add("@NgayMuon", SqlDbType.Date).Value = ms.NgayMuon;
                cmd.Parameters.Add("@NgayHenTra", SqlDbType.Date).Value = ms.NgayHenTra;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool DeleteMuonSach(MuonSach sach)
        {
            string sql = "DELETE tblMuonTraSach WHERE MaDG=@MaDG and MaSach=@MaSach ";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaDG", SqlDbType.Char).Value = sach.MaDG;
                cmd.Parameters.Add("@MaSach", SqlDbType.Char).Value = sach.MaSach;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }


    }
}
