﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CK_CNXMK_Nhom19_Quanlythuvien.DAL
{
    class CapNhatLoaiSachDAL
    {
        Dataconnection tc;
        SqlDataAdapter da;
        SqlCommand cmd;
        public CapNhatLoaiSachDAL()
        {
            tc = new Dataconnection();
        }

        public DataTable getLoaiSach()
        {
            //tạo lệnh sql
            string sql = "SELECT * from tblLoaiSach";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }

        public bool InserttblLoaiSach(LoaiSach loaisach)
        {
            string sql = "INSERT INTO tblLoaiSach(MaLoaiSach,TenLoai,KieuSach) VALUES(@MaLoaiSach,@TenLoai,@KieuSach)";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Char).Value = loaisach.MaLoaiSach;
                cmd.Parameters.Add("@TenLoai", SqlDbType.NVarChar).Value = loaisach.TenLoai;
                cmd.Parameters.Add("@KieuSach", SqlDbType.NVarChar).Value = loaisach.KieuSach;

                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool UpdatetblLoaiSach(LoaiSach loaisach)
        {
            string sql = "UPDATE tblLoaiSach SET MaLoaiSach= @MaLoaiSach, TenLoai=@TenLoai, KieuSach=@KieuSach WHERE MaLoaiSach=@MaLoaiSach";

            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Char).Value = loaisach.MaLoaiSach;
                cmd.Parameters.Add("@TenLoai", SqlDbType.NVarChar).Value = loaisach.TenLoai;
                cmd.Parameters.Add("@KieuSach", SqlDbType.NVarChar).Value = loaisach.KieuSach;
               
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
        public bool DELETEtblLoaiSach(LoaiSach loaisach)
        {
            string sql = "DELETE tblLoaiSach WHERE MaLoaiSach=@MaLoaiSach";
            SqlConnection con = tc.getconnect();
            try
            {
                cmd = new SqlCommand(sql, con);
                con.Open();
                cmd.Parameters.Add("@MaLoaiSach", SqlDbType.Char).Value = loaisach.MaLoaiSach;
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception)
            {
                return false;
            }
            return true;

        }
        public DataTable FindtblLoaiSach(string loaisach)
        {
            string sql = "SELECT * FROM tblLoaiSach WHERE MaLoaiSach like '%" + loaisach + "%' or TenLoai '%" + loaisach + "%'";
            //kết nối sql
            SqlConnection con = tc.getconnect();
            //khơi tạo đói tượng lớp sqldata
            da = new SqlDataAdapter(sql, con);
            con.Open();
            DataTable dt = new DataTable();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}

