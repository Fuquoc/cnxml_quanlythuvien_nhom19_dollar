﻿
create database QuanLiThuVien
use QuanLiThuVien
go
create table taikhoan
(  
  Id varchar(50) primary key,
  MatKhau char(13),
  Quyen varchar(20),
  TenNV nvarchar(50),
  SoDienThoai varchar(10),
  DiaChi nvarchar(50)
)
insert into taikhoan
values
('20505031200261','quoclak02','1',N'Đàm Phú Quốc','0777456902',N'Đà Nẵng'),
('20505031200249','happne','1',N'Lê Phan Hoàng Phúc','0906593171',N'Đà Nẵng'),
('admin','1','1',N'Đàm Phú Quốc','0777456902',N'Đà Nẵng');
create table tblDocGia
(
   MaDG char(13) PRIMARY KEY,
   TenDG nvarchar(50) not null,
   GioiTinh nvarchar(6) null check(GioiTinh in(N'Nữ',N'Nam')),
   Lop nvarchar(30) not null,
   NgayMuon date 
)
insert into tblDocGia
values
('2050531200230',N'Đàm Phú Quốc',N'Nam','20T2','2021-02-01'),
('2050531200231',N'Lê Phan Hoàng Phúc',N'Nam','20T2','2021-02-21'),
('2050531200232',N'Trần Thị Kim Ngân',N'Nữ','20T2','2021-02-21'),
('2050531200233',N'Lê Phan Hoàng Ngân',N'Nữ','20T2','2019-08-01'),
('2050531200134',N'Trần Thị Kim Phúc',N'Nam','20T2','2019-08-21'),
('2050531200135',N'Đàm Phú Phúc',N'Nam','20T2','2021-02-20');
create table tblTacGia
(
	MaTG char(6) primary key,
	TenTG nvarchar(50) ,
	DiaChi nvarchar(50)
)
insert into tblTacGia
values
('TG0001',N'Lữ Thị Mai',N'Thanh Hóa'),
('TG0002',N'Nam Cao',N'Hà Nam'),
('TG0003',N'Lê Hữu Trác',N'Hưng Yên'),
('TG0004',N'Lương Thế Vinh',N'Nam Định'),
('TG0005',N'Hồ Chí Minh',N'Nghệ An'),
('TG0006',N'Nguyễn Văn Dung',N'Đồng Nai'),
('TG0007',N' Phạm Hữu Khang ',N'Hà Nội');

create table tblLoaiSach
(
	MaLoaiSach  char(6) primary key,
	TenLoai nvarchar(40),
	KieuSach nvarchar(40) 
)
insert into tblLoaiSach
values
('TH',N'Tin Học',N'Kiểu Sách'),
('TC',N'Tạp Chí',N'Báo'),
('SGK',N'Sách Giáo Khoa',N'Sách'),
('TT',N'Truyện Tranh',N'Truyện'),
('KT',N'Kinh Tế',N'Sách');

create table tblSach
(
	MaSach char(5) primary key,
	TenSach nvarchar(50) ,
	MaLoaiSach char(6) not null foreign key references tblLoaiSach(MaLoaiSach),
	MaTG char(6) not null foreign key references tblTacGia(MaTG),
	SoLuong int 
 )
 insert into tblSach
 values
 ('S0001',N'Kinh Tế Vĩ Mô','KT','TG0006',40),
 ('S0002',N'Văn Học','SGK','TG0002',50),
 ('S0003',N'Cơ Sở Dữ Liệu','TH','TG0007',60),
 ('S0004',N'Sức Khỏe và Đời Sống','TC','TG0003',50),
 ('S0005',N'Lập Trình Trực Quan','TH','TG0007',60),
 ('S0006',N'Thần Đồng Đất Việt','TT','TG0001',34);
 
 create table tblMuonTraSach
 (
  MaDG char(13) foreign key references  tblDocGia(MaDG),
  MaSach char(5) foreign key references tblSach(MaSach),
    primary key(MaDG,MaSach),
	SoLuong int ,
	NgayMuon date default getdate() check (NgayMuon <= getdate()),
	NgayHenTra date,
	NgayTra date
 )


 alter table tblMuonTraSach
   add constraint ck_NgayHenTra check ( NgayHenTra>=NgayMuon)
 alter table tblMuonTraSach
 add constraint ck_NgayTra check ( NgayTra>=NgayMuon)

delete from tblMuonTraSach
insert into tblMuonTraSach
 values
 ('2050531200230','S0002',3,'11/05/2019','02/05/2021','02/10/2021'),
 ('2050531200230','S0001',2,'05/02/2021','08/21/2021','08/19/2021'),
 ('2050531200231','S0003',1,'05/20/2021','07/20/2021','07/25/2021'),
 ('2050531200232','S0004',2,'03/19/2021','07/19/2021','07/10/2021');
